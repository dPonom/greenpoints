//
//  Preview.h
//  Greenpoints
//
//  Created by Developer on 21.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Preview : NSObject

@property (strong, nonatomic) NSString * company_id;

+ (id)sharedManager;

@end
