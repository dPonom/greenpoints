//
//  VideoInstructionsViewController.m
//  Greenpoints
//
//  Created by Developer on 22.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "VideoInstructionsViewController.h"
#import "EndSurveyViewController.h"
#import "APLSLideMenuViewController.h"

@interface VideoInstructionsViewController ()
@property (weak, nonatomic) IBOutlet UITextView *instructionTextView;

@end

@implementation VideoInstructionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.instructionTextView.text = @"Stap 1:   Film op het bedrijf\n\nVerwerk in het filmpje de volgende elementen:\n\n          1. Laat de ondernemer zich kort voorstellen en vertellen over zijn/haar visie voor het bedrijf en de toekomstplannen. Film dit\n          2. Breng in beeld wat de ondernemer produceert of doet.  Maak bijvoorbeeld een aantal korte shots, opnames of foto´s om te verwerken in de film\n          3. Breng in beeld waarom dit bedrijf bijzonder is (de Unique Selling Points): Waarin onderscheidt de ondernemer zich?  Dit kan van alles zijn, van de manier waarop geproduceerd wordt tot de persoonlijkheid van de ondernemer. Ga hierbij niet alleen af op wat de ondernemer zelf zegt maar gebruik ook je eigen oordeel: Wat valt je op tijdens het bezoek? Film dit ook of breng dit op een andere manier visueel in kaartn\n\nStap 2:   Bewerk de film(pjes) tot één representatieve bedrijfsfilm\n\nStap 3:   Verstuur de film via Googledrive (https://drive.google.com/).  Volg de link en klik in het scherm op 'nieuw: bestanden uploaden'. Kies het juiste bestand. Kies voor bestand delen. Je mag dit delen met greenpointsnl@gmail.com\nSchrijf de naam van het bedrijf op als titel van het bestand. Klik op verzenden\n\nStap 4:   Log in via de app en kopieer de youtube link in de vragenlijst op de juiste plek\n\nStap 5:   Misschien win jij wel de film-wedstrijd en ga je naar huis met mooie prijzen!";
    
    self.instructionTextView.contentOffset = CGPointMake(0, 0);
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
    
    
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)nextButton:(id)sender
{
    EndSurveyViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EndSurveyViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
