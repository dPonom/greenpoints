//
//  SevenQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SevenQuestionViewController.h"
#import "EightQuestionViewController.h"
#import "PopoverViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface SevenQuestionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *mainQuestionLabel;
@property (weak, nonatomic) IBOutlet UIButton *yesVerticalButton;
- (IBAction)yesVerticalButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *verticalTextField;
@property (weak, nonatomic) IBOutlet UIButton *yesHorisontalButton;
- (IBAction)yesHorisontalButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *horisontalTextField;
- (IBAction)noAloneButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *noAloneButton;
@property (weak, nonatomic) IBOutlet UILabel *potentialPartnersLabel;
- (IBAction)yesPartnersButton:(id)sender;
- (IBAction)noPartnersButton:(id)sender;
- (IBAction)dontKnowPartnersButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *yesPartnersBtn;
@property (weak, nonatomic) IBOutlet UIButton *noPartnersBtn;
@property (weak, nonatomic) IBOutlet UIButton *dontKnowPartnersBtn;
@property (weak, nonatomic) IBOutlet UIImageView *yesPartnersChekmark;
@property (weak, nonatomic) IBOutlet UIImageView *noPartnersChekmark;
@property (weak, nonatomic) IBOutlet UIImageView *dontKnowPartnersChekmark;
@property (weak, nonatomic) IBOutlet UILabel *whatAreyouWaitngLabel;
@property (weak, nonatomic) IBOutlet UITextField *whatYouWaitTextField;
@property (weak, nonatomic) IBOutlet UILabel *troublesLabel;
@property (weak, nonatomic) IBOutlet UITextField *troublesTextField;
@property (weak, nonatomic) IBOutlet UILabel *ideaslabel;
- (IBAction)yesIdeasButton:(id)sender;
- (IBAction)noIdeasButton:(id)sender;
- (IBAction)dontKnowIdeasButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *yesIdeasChekmark;
@property (weak, nonatomic) IBOutlet UIImageView *noIdeasChekmark;
@property (weak, nonatomic) IBOutlet UIImageView *dontKnowIdeasChekmark;
- (IBAction)nextButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *popupView;
- (IBAction)firstNextButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;

@property (assign,nonatomic) BOOL alreadyOpen;

- (IBAction)infoButton:(id)sender;

@property (strong,nonatomic) NSString * answer;

@property (assign,nonatomic) NSInteger value;

@end

@implementation SevenQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    self.potentialPartnersLabel.hidden = YES;
    self.yesIdeasChekmark.hidden = YES;
    self.noPartnersChekmark.hidden = YES;
    self.dontKnowPartnersChekmark.hidden = YES;
    self.yesPartnersBtn.hidden = YES;
    self.noPartnersBtn.hidden = YES;
    self.dontKnowPartnersBtn.hidden = YES;
    self.popupView.hidden = YES;
    self.verticalTextField.hidden = YES;
    self.horisontalTextField.hidden = YES;
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark Keyboard Settings

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification object:nil];
}

// Unsubscribe from keyboard show/hide notifications.
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWillShow:(NSNotification*)notification
{
    if(self.alreadyOpen == YES)
    {
        return;
    }
    else
    {
        self.alreadyOpen = YES;
        [self moveView:[notification userInfo] up:YES];
    }
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    self.alreadyOpen = NO;
    [self moveView:[notification userInfo] up:NO];
}

- (void)moveView:(NSDictionary*)userInfo up:(BOOL)up
{
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]
     getValue:&keyboardEndFrame];
    
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey]
     getValue:&animationCurve];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey]
     getValue:&animationDuration];
    
    // Get the correct keyboard size to we slide the right amount.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    int y = keyboardFrame.size.height * (up ? -1 : 1);
    self.view.frame = CGRectOffset(self.view.frame, 0, y);
    
    [UIView commitAnimations];
}



- (IBAction)yesVerticalButton:(id)sender
{
    self.verticalTextField.hidden = NO;
    self.horisontalTextField.hidden = YES;
    
    self.firstBtn.hidden = YES;
    self.potentialPartnersLabel.hidden = NO;
    self.yesIdeasChekmark.hidden = NO;
    self.noPartnersChekmark.hidden = NO;
    self.dontKnowPartnersChekmark.hidden = NO;
    self.yesPartnersBtn.hidden = NO;
    self.noPartnersBtn.hidden = NO;
    self.dontKnowPartnersBtn.hidden = NO;
    self.popupView.hidden = NO;
    
    self.value = 1;
    
  
}
- (IBAction)yesHorisontalButton:(id)sender
{
    self.horisontalTextField.hidden = NO;
    self.verticalTextField.hidden = YES;
    
    self.firstBtn.hidden = YES;
    self.potentialPartnersLabel.hidden = NO;
    self.yesIdeasChekmark.hidden = NO;
    self.noPartnersChekmark.hidden = NO;
    self.dontKnowPartnersChekmark.hidden = NO;
    self.yesPartnersBtn.hidden = NO;
    self.noPartnersBtn.hidden = NO;
    self.dontKnowPartnersBtn.hidden = NO;
    self.popupView.hidden = NO;
    
    self.value = 2;
    

}
- (IBAction)noAloneButton:(id)sender
{
    self.firstBtn.hidden = NO;
    self.potentialPartnersLabel.hidden = YES;
    self.yesIdeasChekmark.hidden = YES;
    self.noPartnersChekmark.hidden = YES;
    self.dontKnowPartnersChekmark.hidden = YES;
    self.yesPartnersBtn.hidden = YES;
    self.noPartnersBtn.hidden = YES;
    self.dontKnowPartnersBtn.hidden = YES;
    self.popupView.hidden = YES;
    self.verticalTextField.hidden = YES;
    self.horisontalTextField.hidden = YES;
    
    self.value = 3;
    
}
- (IBAction)yesPartnersButton:(id)sender
{
    [self.yesPartnersChekmark setImage:[UIImage imageNamed:@"Checked.png"]];
    [self.noPartnersChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.dontKnowPartnersChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
}

- (IBAction)noPartnersButton:(id)sender
{
    [self.yesPartnersChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.noPartnersChekmark setImage:[UIImage imageNamed:@"Checked.png"]];
    [self.dontKnowPartnersChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
}

- (IBAction)dontKnowPartnersButton:(id)sender
{
    [self.yesPartnersChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.noPartnersChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.dontKnowPartnersChekmark setImage:[UIImage imageNamed:@"Checked.png"]];
}
- (IBAction)yesIdeasButton:(id)sender
{
    [self.yesIdeasChekmark setImage:[UIImage imageNamed:@"Checked.png"]];
    [self.noIdeasChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.dontKnowIdeasChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
}

- (IBAction)noIdeasButton:(id)sender
{
    [self.yesIdeasChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.noIdeasChekmark setImage:[UIImage imageNamed:@"Checked.png"]];
    [self.dontKnowIdeasChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
}

- (IBAction)dontKnowIdeasButton:(id)sender
{
    [self.yesIdeasChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.noIdeasChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.dontKnowIdeasChekmark setImage:[UIImage imageNamed:@"Checked.png"]];
}
- (IBAction)nextButton:(id)sender
{
    switch (self.value)
    {
        case 1:
            self.answer = [NSString stringWithFormat:@"Vertical - %@",self.verticalTextField.text];
            break;
        case 2:
            self.answer = [NSString stringWithFormat:@"Horisontal - %@",self.horisontalTextField.text];
            break;
        case 3:
            self.answer = @"Nee, ik werk niet samen";
            break;
            
        default:
            break;
    }
    
    NSDictionary * dict = @{@"question":@"7. Werkt u met andere ondernemers samen?",
                            @"answer":@[self.answer]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:7];
    
    EightQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EightQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)firstNextButton:(id)sender
{
    switch (self.value)
    {
        case 1:
            self.answer = [NSString stringWithFormat:@"Vertical - %@",self.verticalTextField.text];
            break;
        case 2:
            self.answer = [NSString stringWithFormat:@"Horisontal - %@",self.horisontalTextField.text];
            break;
        case 3:
            self.answer = @"Nee, ik werk niet samen";
            break;
            
        default:
            break;
    }
    
    NSDictionary * dict = @{@"question":@"7. Werkt u met andere ondernemers samen?",
                            @"answer":@[self.answer]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:7];
    
    EightQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EightQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 5;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
