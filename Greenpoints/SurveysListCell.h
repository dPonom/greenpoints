//
//  SurveysListCell.h
//  Greenpoints
//
//  Created by Developer on 22.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveysListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyPhoneLabel;

@end
