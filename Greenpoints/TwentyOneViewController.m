//
//  TwentyOneViewController.m
//  Greenpoints
//
//  Created by Developer on 31.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "TwentyOneViewController.h"
#import "EndSurveyViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"
#import "VideoInstructionsViewController.h"

@interface TwentyOneViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (strong, nonatomic) NSString * string;

@property (strong,nonatomic) NSString * answer;
@property (assign ,nonatomic) NSInteger value;

@end

@implementation TwentyOneViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    self.descriptionTextView.hidden = YES;
    self.descriptionTextView.text = @"Uw antwoord...";
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

#pragma mark UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(!self.string)
    {
        self.descriptionTextView.text = @"";
    }
    else
    {
        self.descriptionTextView.text = self.string;
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.string = self.descriptionTextView.text;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark Actions

- (IBAction)noButton:(id)sender
{
    [self.noButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.yesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.descriptionTextView.hidden = YES;
    
    self.value = 1;
}

- (IBAction)yesButton:(id)sender
{
    [self.noButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.yesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.descriptionTextView.hidden = NO;
    
    
    self.value = 2;
}

- (IBAction)nextButton:(id)sender
{
    switch (self.value)
    {
        case 1:
            self.answer = @"Nee";
            break;
        case 2:
            self.answer = [NSString stringWithFormat:@"Ja - %@",self.descriptionTextView.text];
            break;
            
        default:
            break;
    }
    NSDictionary * dict = @{@"question":@"Wilt u nog iets toevoegen?",
                            @"answer":@[self.answer]};
    
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:21];
    
    VideoInstructionsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoInstructionsViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
