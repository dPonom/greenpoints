//
//  SixQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SixQuestionViewController.h"
#import "SevenQuestionViewController.h"
#import "PopoverViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface SixQuestionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *sixQuestionLabel;
- (IBAction)yesButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *detailTextField;
- (IBAction)noButton:(id)sender;
- (IBAction)dontKnowButton:(id)sender;
- (IBAction)nextButton:(id)sender;
- (IBAction)infoButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *dontKnowButton;

@property (strong, nonatomic) NSString * answer;

@property (assign, nonatomic) NSInteger value;

@end

@implementation SixQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    self.detailTextField.hidden = YES;
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)yesButton:(id)sender
{
    self.detailTextField.hidden = NO;
    self.value = 1;
    
    
    [self.yesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.noButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.dontKnowButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}
- (IBAction)noButton:(id)sender
{
    self.detailTextField.hidden = YES;
    self.value = 2;
    
    [self.yesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.noButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.dontKnowButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

- (IBAction)dontKnowButton:(id)sender
{
    self.detailTextField.hidden = YES;
    self.value = 3;
    
    [self.yesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.noButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.dontKnowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)nextButton:(id)sender
{
    switch (self.value)
    {
        case 1:
            self.answer = [NSString stringWithFormat:@"Ja - %@",self.detailTextField.text];
            break;
        case 2:
            self.answer = @"Nee";
            break;
        case 3:
            self.answer = @"Weet ik niet";
            break;
            
        default:
            break;
    }
    NSDictionary * dict = @{@"question":@"6. Heeft u in uw bedrijf afval of reststromen die u kwijt moet?",
                            @"answer":@[self.answer]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:6];
    
    SevenQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SevenQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 4;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
