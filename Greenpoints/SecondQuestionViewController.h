//
//  SecondQuestionViewController.h
//  Greenpoints
//
//  Created by Developer on 22.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondQuestionViewController : UIViewController

@property (assign ,nonatomic) NSInteger status;
@property (strong, nonatomic) NSMutableArray * multipleAnswers;

@end
