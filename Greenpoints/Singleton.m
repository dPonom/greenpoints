//
//  Singleton.m
//  Greenpoints
//
//  Created by Dmitriy Ponomarenko on 07.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

#pragma mark Singleton Methods

+ (id)sharedManager {
    static Singleton *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.products = [[NSMutableArray alloc]init];
        sharedMyManager.activity = [[NSMutableArray alloc]init];
        sharedMyManager.answers = [[NSMutableArray alloc]init];
        sharedMyManager.search = [[NSString alloc] init];
        sharedMyManager.user_id = [[NSString alloc] init];
        sharedMyManager.token = [[NSString alloc] init];
    });
    return sharedMyManager;
}




@end
