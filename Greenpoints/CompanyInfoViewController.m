//
//  CompanyInfoViewController.m
//  Greenpoints
//
//  Created by павев on 04.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CompanyInfoViewController.h"
#import "SurveyStartViewController.h"
#import "CurrentUser+CoreDataProperties.h"
#import "Company+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "NetworkService.h"

@interface CompanyInfoViewController ()
{
    UIDatePicker * datePicker;
    UIDatePicker * timePicker;
}
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *whenLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;
@property (weak, nonatomic) IBOutlet UIButton *unknownDate;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (weak, nonatomic) IBOutlet UITextField *timeTextField;
@property (weak, nonatomic) IBOutlet UITextView *contactInfoTextView;
@property (weak, nonatomic) IBOutlet UITextView *workSheludeTextView;

@property (strong, nonatomic) NSString * previewAnswer;

@property (assign, nonatomic) BOOL dateIsUnknown;

@property (assign,nonatomic) BOOL alreadyOpen;

@property (strong, nonatomic) NSString * dateOfMeeting;


@end

@implementation CompanyInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addTimePicker];
    
    self.contactInfoTextView.text = @"";
    self.workSheludeTextView.text = @"";
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    datePicker = [[ UIDatePicker alloc ] init ];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:30];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-30];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:maxDate];
    [datePicker setMinimumDate:minDate];
    
    [self.dateTextField setInputView:datePicker];
    
    UIToolbar * toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem * doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(showSelectedDate)];
    UIBarButtonItem * space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                            target:nil
                                                                            action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn,nil]];
    [self.dateTextField setInputAccessoryView:toolBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.dateIsUnknown = NO;
    [self.statusImage setImage:[UIImage imageNamed:@"notCheked.png"]];
}

#pragma mark UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark Keyboard Settings

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveUpView:)
                                                 name:UITextViewTextDidBeginEditingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveDownView:)
                                                 name:UITextViewTextDidEndEditingNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidBeginEditingNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidEndEditingNotification
                                                  object:nil];
}

-(void) moveUpView:(NSNotification*)notification
{
    if(self.workSheludeTextView == notification.object)
    {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{self.view.frame = CGRectOffset(self.view.frame, 0, -120);}
                         completion:^(BOOL finished) {}];
    }
}

-(void) moveDownView:(NSNotification*)notification
{
    if(self.workSheludeTextView == notification.object)
    {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut
                         animations:^{self.view.frame = CGRectOffset(self.view.frame, 0, +120);}
                         completion:^(BOOL finished) {}];
    }
}



-(void) showSelectedDate
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    self.dateTextField.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [self.dateTextField resignFirstResponder];
}

-(void) showSelectedTime
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    self.timeTextField.text = [NSString stringWithFormat:@"%@",[formatter stringFromDate:timePicker.date]];
    [self.timeTextField resignFirstResponder];
}

- (IBAction)unknownDate:(id)sender
{
    self.dateIsUnknown = YES;
    [self.statusImage setImage:[UIImage imageNamed:@"Checked.png"]];
    self.dateTextField.text = @"";
    self.timeTextField.text = @"";
    [self.dateTextField resignFirstResponder];
    [self.timeTextField resignFirstResponder];
}

-(void) addTimePicker
{
    timePicker = [[ UIDatePicker alloc ] init ];
    timePicker.datePickerMode = UIDatePickerModeTime;
    
    [self.timeTextField setInputView:timePicker];
    
    UIToolbar * toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem * doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(showSelectedTime)];
    UIBarButtonItem * space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                            target:nil
                                                                            action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn,nil]];
    [self.timeTextField setInputAccessoryView:toolBar];
}

- (IBAction)nextButton:(id)sender
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if((self.contactInfoTextView.text.length == 0) || ([[self.contactInfoTextView.text stringByTrimmingCharactersInSet: set] length] == 0) ||
       (self.workSheludeTextView.text.length == 0) || ([[self.workSheludeTextView.text stringByTrimmingCharactersInSet: set] length] == 0))
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Foutmelding!"
                                                                       message:@"Alle velden moeten worden ingevuld"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
                                        {
                                        }];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert animated:YES completion:nil];

    }
    else
    {
        if(self.dateIsUnknown == YES)
        {
            self.dateOfMeeting = @"datum/tijd staat nog niet vast";
            
            self.previewAnswer = [NSString stringWithFormat:@"Ja, het is gelukt om een afspraak te maken met het bedrijf , %@ ,Contactpersoon - %@,Functie in bedrijf - %@",
                                  self.dateOfMeeting,self.contactInfoTextView.text, self.workSheludeTextView.text];
            
            [self sendPreviewToServer];
        }
        else
        {
            if((self.dateTextField.text.length == 0) || ([[self.dateTextField.text stringByTrimmingCharactersInSet: set] length] == 0) ||
               (self.timeTextField.text.length == 0) || ([[self.timeTextField.text stringByTrimmingCharactersInSet: set] length] == 0) ||
               (!self.dateTextField.text) || (!self.timeTextField.text))
            {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Foutmelding!"
                                                                               message:@"Alle velden moeten worden ingevuld"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action)
                                                {
                                                }];
                
                [alert addAction:defaultAction];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                self.dateOfMeeting = [NSString stringWithFormat:@"datum - %@, tijd - %@",self.dateTextField.text, self.timeTextField.text];
                
                self.previewAnswer = [NSString stringWithFormat:@"Ja, het is gelukt om een afspraak te maken met het bedrijf , %@ ,Contactpersoon - %@,Functie in bedrijf - %@",
                                      self.dateOfMeeting,self.contactInfoTextView.text, self.workSheludeTextView.text];
                
                [self sendPreviewToServer];
            }
        }
    }
}

-(void) sendPreviewToServer
{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = NSLocalizedString(@"Saving Preview", @"HUD loading title");
    
    CurrentUser * user = [CurrentUser MR_findFirst];
    Company * company  = [[Company MR_findAll] lastObject];
    
    NSDictionary * params = @{@"user_id"       : user.userID,
                              @"token"         : user.userToken,
                              @"company_id"    : company.companyID,
                              @"survey_status" : self.previewAnswer };
    [[NetworkService sharedService] sendPreviewResult:params andBlock:^(id responseObject, NSError *error)
     {
         if([responseObject[@"success"] isEqual:@1])
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             UINavigationController * nav = [self.storyboard instantiateViewControllerWithIdentifier:@"startSurvey"];
             [self presentViewController:nav animated:YES completion:nil];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             NSLog(@"error of sending preview to server");
         }
     }];
}

@end
