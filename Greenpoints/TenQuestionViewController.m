//
//  TenQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "TenQuestionViewController.h"
#import "ElevenQuestionViewController.h"
#import "PopoverViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface TenQuestionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *mainQuestionLabel;
@property (weak, nonatomic) IBOutlet UIButton *haveNotBtn;
- (IBAction)haveNotBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dontKnowBtn;
- (IBAction)dontKnowBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *myUspBtn;
- (IBAction)myUspBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *uspTextField;
- (IBAction)nextButton:(id)sender;
- (IBAction)infoButton:(id)sender;

@property (strong ,nonatomic) NSString * answer;
@property (assign,nonatomic) NSInteger value;

@end

@implementation TenQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)haveNotBtn:(id)sender
{
    [self.haveNotBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.dontKnowBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.myUspBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    self.value = 1;
}

- (IBAction)dontKnowBtn:(id)sender
{
    [self.haveNotBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.dontKnowBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.myUspBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    self.value = 2;
}

- (IBAction)myUspBtn:(id)sender
{
    [self.haveNotBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.dontKnowBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.myUspBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.value = 3;
}

- (IBAction)nextButton:(id)sender
{
    switch (self.value)
    {
        case 1:
            self.answer = @"Ik neb geen usp";
            break;
        case 2:
            self.answer = @"Weet ik niet";
            break;
        case 3:
            self.answer = [NSString stringWithFormat:@"Mijn usp zijn - %@", self.uspTextField.text];
            break;
            
        default:
            break;
    }
    NSDictionary * dict = @{@"question":@"10. Wat zijn volgens u uw unique selling points (‘usp’)?",
                            @"answer":@[self.answer]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:10];
    
    ElevenQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ElevenQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 8;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
