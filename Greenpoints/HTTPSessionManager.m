//
//  DPSessionManager.m
//  Finance
//
//  Created by Admin on 20.05.16.
//  Copyright © 2016 diP. All rights reserved.
//

#import "HTTPSessionManager.h"

@implementation HTTPSessionManager

- (NSURLSessionDataTask *)HTTPSessionWithHTTPRequestType:(HTTPRequestType)type
                                               URLString:(NSString *)URLString
                                              parameters:(id)parameters
                                                progress:(void (^)(NSProgress * _Nonnull))progress
                                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    switch (type)
    {
        case HTTPRequestTypeGET:
        {
            return [self GET:URLString parameters:parameters progress:progress success:success failure:failure];
        }
        case HTTPRequestTypePUT:
        {
            return [self PUT:URLString parameters:parameters success:success failure:failure];
        }
        case HTTPRequestTypePOST:
        {
            return [self POST:URLString parameters:parameters progress:progress success:success failure:failure];
        }
        case HTTPRequestTypeDELETE:
        {
            return [self DELETE:URLString parameters:parameters success:success failure:failure];
        }
    }
}

@end
