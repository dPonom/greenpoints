//
//  DPSessionManager.h
//  Finance
//
//  Created by Admin on 20.05.16.
//  Copyright © 2016 diP. All rights reserved.
//

#import "AFNetworking/AFNetworking.h"

typedef NS_ENUM(NSInteger, HTTPRequestType)
{
    HTTPRequestTypeGET,
    HTTPRequestTypePOST,
    HTTPRequestTypePUT,
    HTTPRequestTypeDELETE
};

@interface HTTPSessionManager : AFHTTPSessionManager

- (NSURLSessionDataTask *)HTTPSessionWithHTTPRequestType:(HTTPRequestType)type
                                               URLString:(NSString *)URLString
                                              parameters:(id)parameters
                                                progress:(void (^)(NSProgress *progress))progress
                                                 success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                                 failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

@end
