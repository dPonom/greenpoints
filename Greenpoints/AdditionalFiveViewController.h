//
//  AdditionalFiveViewController.h
//  Greenpoints
//
//  Created by Dmitriy Ponomarenko on 20.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdditionalFiveViewController : UIViewController

@property (assign,nonatomic) NSInteger number;

@end
