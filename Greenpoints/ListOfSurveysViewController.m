//
//  ListOfSurveysViewController.m
//  Greenpoints
//
//  Created by Developer on 22.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ListOfSurveysViewController.h"
#import "APLSlideMenuViewController.h"
#import "SurveysListCell.h"
#import "DetailSurveyViewController.h"
#import "Singleton.h"
#import "NetworkService.h"

#import "MBProgressHUD.h"
#import <MagicalRecord/MagicalRecord.h>
#import "CurrentUser+CoreDataProperties.h"
#import "Company+Saving.h"

@interface ListOfSurveysViewController () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray * companies;

@end

@implementation ListOfSurveysViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.companies = [[NSMutableArray alloc] init];

    [self getListOfSurveysForCurrentUser];
  
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Mijn vragenlijsten"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"backForNavBar.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    
    UIImage *image = [UIImage imageNamed:@"backButton2.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, 25 , 25);
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *plusButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = plusButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.companies count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SurveysListCell * cell = [tableView dequeueReusableCellWithIdentifier:@"surveyList"];
    
    cell.companyNameLabel.text = [[self.companies objectAtIndex:indexPath.row] companyName];
    cell.companyAddressLabel.text = [[self.companies objectAtIndex:indexPath.row] companyAddress];
    cell.companyPhoneLabel.text = [[self.companies objectAtIndex:indexPath.row] companyPhone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailSurveyViewController * detail = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailSurveyViewController"];
    detail.company_id = [[self.companies objectAtIndex:indexPath.row] companyID];
    [self.navigationController pushViewController:detail animated:YES];
}

-(IBAction)backButton:(id)sender
{
    APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
    [self presentViewController:apl animated:YES completion:nil];
}

- (void) getListOfSurveysForCurrentUser
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    CurrentUser * user = [CurrentUser MR_findFirst];
    
    NSDictionary * params = @{@"user_id" : user.userID,
                              @"token"   : user.userToken };
    [[NetworkService sharedService] getSurveysOfCurrentUserWithParams:params andBlock:^(id responseObject, NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSArray * companiesFromServer = [responseObject objectForKey:@"companies"];
         for (id obj in companiesFromServer)
         {
             Company * comp = [Company MR_createEntity];
             
             comp.companyAddress        = [obj objectForKey:@"address"];
             comp.companyName           = [obj objectForKey:@"company"];
             comp.companyID             = [NSString stringWithFormat:@"%@",[obj objectForKey:@"company_id"]];
             comp.companyPhone          = [obj objectForKey:@"phone"];
             comp.companySBI            = [obj objectForKey:@"sbi"];
             comp.companyYouTubeLink    = [obj objectForKey:@"youtube_link"];
             
             [self.companies addObject:comp];
         }
         
         [self.tableView reloadData];
     }];
}



@end
