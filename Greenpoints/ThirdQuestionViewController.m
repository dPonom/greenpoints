//
//  ThirdQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 23.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ThirdQuestionViewController.h"
#import "CustomCell.h"
#import "FourQuestionViewController.h"
#import "DetailsViewController.h"
#import "PopoverViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface ThirdQuestionViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *multipleAnswersLabel;
@property (weak, nonatomic) IBOutlet UITextField *detailsTextField;
- (IBAction)nextButton:(id)sender;

@property (strong, nonatomic) NSMutableArray * titlesOfRows;

@property (strong, nonatomic) NSMutableArray * arrayOfArrays;
@property (strong, nonatomic) NSMutableArray * array1;
@property (strong, nonatomic) NSMutableArray * array2;
@property (strong, nonatomic) NSMutableArray * array3;
@property (strong, nonatomic) NSMutableArray * array4;
@property (strong, nonatomic) NSMutableArray * array5;
@property (strong, nonatomic) NSMutableArray * array6;
@property (strong, nonatomic) NSMutableArray * array7;
@property (strong, nonatomic) NSMutableArray * array8;
@property (strong, nonatomic) NSMutableArray * array9;
@property (strong, nonatomic) NSMutableArray * array10;

- (IBAction)infoButton:(id)sender;
- (IBAction)questionButton:(id)sender;

@property (strong, nonatomic) NSString * currentAnswer;
@property (strong, nonatomic) NSIndexPath * latestSelectedIndexPath;

@end

@implementation ThirdQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.titlesOfRows = [[NSMutableArray alloc] init];
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self initArrayForAnswers];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"backForNavBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.tableView.editing = YES;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    return [self.multipleAnswers count];
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString * sectionName = [[NSString alloc] init];
    //self.tableView
    
    //[sectionName setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:18.0]];
    NSInteger  number = [[self.multipleAnswers objectAtIndex:section] integerValue];
    
    switch (number)
    {
        case 0:
            sectionName = @"Primaire producent";
            return sectionName;
            break;
        case 1:
            sectionName = @"Verwerker";
            return sectionName;
            break;
        case 2:
            sectionName = @"Transporteur";
            return sectionName;
            break;
        case 3:
            sectionName = @"Groothandel";
            return sectionName;
            break;
        case 4:
            sectionName = @"Instellingskeuken";
            return sectionName;
            break;
        case 5:
            sectionName = @"Detailhandel";
            return sectionName;
            break;
        case 6:
            sectionName = @"Loonwerker";
            return sectionName;
            break;
        case 7:
            sectionName = @"Toeleverancier";
            return sectionName;
            break;
        case 8:
            sectionName = @"Dienstverlener";
            return sectionName;
            break;
            
        default:
            sectionName = @"zijn versie";
            return sectionName;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CustomCell heightForText:[[self.arrayOfArrays objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.arrayOfArrays objectAtIndex:section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = [[self.arrayOfArrays objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:18.0]];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.latestSelectedIndexPath = indexPath;
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
    NSString * name =  [[self.arrayOfArrays objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self howManyProductsDoYouSell:name];
    
    //[self.titlesOfRows addObject:name];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
    NSString * name=  [[self.arrayOfArrays objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self.titlesOfRows removeObject:name];
}

- (UITableViewCellEditingStyle)tableViewStyle
{
    return 3;
}

- (UITableViewCellEditingStyle)tableView:(UITableView * )tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableViewStyle];
}

#pragma mark Init Array

-(void) initArrayForAnswers
{
    [self initializationOfArrays];
    self.arrayOfArrays = [[NSMutableArray alloc] init];
    for (NSString * obj in self.multipleAnswers)
    {
        NSInteger  number = [obj integerValue];
        switch (number)
        {
            case 0:
                [self.arrayOfArrays addObject:self.array1];
                break;
            case 1:
                [self.arrayOfArrays addObject:self.array2];
                break;
            case 2:
                [self.arrayOfArrays addObject:self.array3];
                break;
            case 3:
                [self.arrayOfArrays addObject:self.array4];
                break;
            case 4:
                [self.arrayOfArrays addObject:self.array5];
                break;
            case 5:
                [self.arrayOfArrays addObject:self.array6];
                break;
            case 6:
                [self.arrayOfArrays addObject:self.array7];
                break;
            case 7:
                [self.arrayOfArrays addObject:self.array8];
                break;
            case 8:
                [self.arrayOfArrays addObject:self.array9];
                break;
                
            default:
                [self.arrayOfArrays addObject:self.array10];
                break;
        }
    }
}


-(void) initializationOfArrays
{
    self.array1 = [[NSMutableArray alloc] initWithObjects:@"koeien: aantal",
                   @"varkens: aantal",
                   @"kippen: aantal",
                   @"melk: kg",
                   @"eieren: kg",
                   @"vis: kg",
                   @"insecten en wormen: kg",
                   @"groente (welke?): kg",
                   @"fruit (welk?): kg",
                   @"aardappelen: ton",
                   @"insecten, wormen",
                   @"groente (welke?)",
                   @"fruit (welk?)",
                   @"aardappelen",
                   @"bieten: ton",
                   @"granen (welke?): ton",
                   @"lupine: ton",
                   @"andere producten, namelijk …: kg",
                   nil];

    self.array2 = [[NSMutableArray alloc] initWithObjects:@"vlees en vleeswaren: aantal artikelen en totaal kg",
                   @"zuivelproducten: aantal artikelen en totaal kg",
                   @"groenteproducten: aantal artikelen en totaal kg",
                   @"fruitproducten: aantal artikelen en totaal kg",
                   @"eierproducten: aantal artikelen en totaal kg",
                   @"vis en schaaldieren: aantal artikelen en totaal kg",
                   @"aardappelproducten: aantal artikelen en totaal kg",
                   @"bietenproducten: aantal artikelen en totaal kg",
                   @"graanproducten: aantal artikelen en totaal kg",
                   @"veganistische producten: aantal artikelen en totaal kg",
                   @"andere producten, namelijk soort, aantal artikelen en totaal kg",
                   nil];
    
    self.array3 = [[NSMutableArray alloc] initWithObjects:@"levende dieren: aantal diersoorten en totaal ton",
                   @"bulkproducten: aantal producten en totaal ton",
                   @"gekoelde of bevroren producten: aantal producten en totaal ton",
                   @"aantal ritten per jaar en totaal aantal  gereden km",
                   @"opgeslagen producten: soort en aantal producten en totaal m3",
                   nil];

    self.array4 = [[NSMutableArray alloc] initWithObjects:@"brood en banket: aantal artikelen en totaal kg",
                   @"vlees en vleeswaren: aantal artikelen en totaal kg",
                   @"groente en fruit: aantal artikelen en totaal kg",
                   @"vis en schaaldieren: aantal artikelen en totaal kg",
                   @"gerechten en maaltijden: aantal artikelen en totaal kg",
                   @"groente- en fruitsappen: aantal artikelen en totaal kg",
                   @"non-food producten: aantal artikelen en totaal kg",
                   @"anders, namelijk: soort, aantal artikelen en totaal kg",
                   nil];

    self.array5 = [[NSMutableArray alloc] initWithObjects:@"aantal ontbijtjes per jaar:",
                   @"aantal lunches per jaar:",
                   @"aantal diners per jaar:",
                   @"aantal snacks per jaar:",
                   @"aantal andere maaltijden per jaar:",
                   nil];

    self.array6 = [[NSMutableArray alloc] initWithObjects:@"brood en banket: aantal artikelen en totaal kg",
                   @"vlees en vleeswaren: aantal artikelen en totaal kg",
                   @"groente en fruit: aantal artikelen en totaal kg",
                   @"vis en schaaldieren: aantal artikelen en totaal kg",
                   @"gerechten en maaltijden: aantal artikelen en totaal kg",
                   @"groente- en fruitsappen: aantal artikelen en totaal kg",
                   @"non-food producten: aantal artikelen en totaal kg",
                   @"anders, namelijk: soort, aantal artikelen en totaal kg",
                   nil];
    
    self.array7 = [[NSMutableArray alloc] initWithObjects:@"hoeveel fte werken in uw bedrijf?",
                   @"hoeveel reststromen ontvangt u (ton per jaar)?",
                   @"hoeveelheid andere goederen, namelijk",
                   nil];
    
    self.array8 = [[NSMutableArray alloc] initWithObjects:@"machines, mechanisering en automatisering: aantal fte in uw bedrijf op dit gebied werkzaam",
                   @"veevoer: ton per jaar",
                   @"meststoffen: ton per jaar",
                   @"zaad-, plant- en pootgoed: voor hoeveel ha per jaar",
                   @"diergeneesmiddelen, gewasbeschermings- en ontsmettingsmiddelen: jaaromzet in euro",
                   @"anders, namelijk welke goederen en hoeveelheden daarvan",
                   nil];
    
    self.array9 = [[NSMutableArray alloc] initWithObjects:@"adviezen: aantal adviesuren per jaar",
                   @"administratieve diensten: aantal fte in uw bedrijf hierin werkzaam",
                   @"controlediensten, verklaringen en certificaten: welke diensten en aantal fte in uw bedrijf hierin werkzaam",
                   @"veterinaire diensten: aantal fte in uw bedrijf hierin werkzaam",
                   @"andere dienstverlening: welke diensten en aantal fte in uw bedrijf hierin werkzaam",
                   nil];
    
    
    self.array10 = [[NSMutableArray alloc] initWithObjects:@"aub hier de hoeveelheid of omzet van het product invullen",nil];
}



#pragma mark Actions

- (IBAction)nextButton:(id)sender
{
    NSDictionary * dict = @{@"question":@"3. Hoeveel produceert of verkoopt u jaarlijks?",
                            @"answer":self.titlesOfRows};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:2];
    
    FourQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FourQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(void) howManyProductsDoYouSell:(NSString *) product {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: product
                                                                              message: nil
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"uw antwoord";
        textField.textColor       = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle     = UITextBorderStyleNone;
    }];
    

    
    [alertController addAction:[UIAlertAction actionWithTitle:@"One More" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             
                                                    
                
                                                         }]];
    
    
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * tf = textfields[0];
        
        NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
        if(tf.text.length == 0 || [[tf.text stringByTrimmingCharactersInSet: set] length] == 0) {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Foutmelding!"
                                                                           message:@"Alle velden moeten worden ingevuld"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                            {
                                                [self.tableView deselectRowAtIndexPath:self.latestSelectedIndexPath animated:NO];
                                            }];
            
            [alert addAction:defaultAction];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            if([product containsString:@"welke"])
            {
                self.currentAnswer = [product stringByReplacingOccurrencesOfString:@"(welke?)" withString:tf.text];
            }
            else if ([product containsString:@"waarvoor"])
            {
                self.currentAnswer = [product stringByReplacingOccurrencesOfString:@"(waarvoor?)" withString:tf.text];
            }
            else
            {
                self.currentAnswer = tf.text;
            }
            
            NSLog(@"%@",self.currentAnswer);
            NSString * new = [[self.arrayOfArrays objectAtIndex:self.latestSelectedIndexPath.section] objectAtIndex:self.latestSelectedIndexPath.row];
            new = [new stringByReplacingOccurrencesOfString:new withString:self.currentAnswer];
            
            
            [self.titlesOfRows addObject:self.currentAnswer];
        }
        
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}




#pragma mark Actions


- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 2;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)questionButton:(id)sender
{
    DetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 2;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
