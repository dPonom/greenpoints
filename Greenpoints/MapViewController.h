//
//  MapViewController.h
//  Greenpoints
//
//  Created by Developer on 16.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapViewController : UIViewController

@property (strong, nonatomic) NSArray * productFilters;
@property (strong, nonatomic) NSArray * activityFilters;


@end
