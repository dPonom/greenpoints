//
//  CustomCell.h
//  Greenpoints
//
//  Created by Developer on 19.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

+(CGFloat) heightForText:(NSString*) text;

@end
