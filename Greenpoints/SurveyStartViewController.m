//
//  SurveyStartViewController.m
//  Greenpoints
//
//  Created by Developer on 19.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SurveyStartViewController.h"
#import "QuestionsViewController.h"
#import "TryDateViewController.h"
#import "NetworkService.h"
#import "Singleton.h"
#import "Preview.h"
#import "SbiCode+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import "CurrentUser+CoreDataClass.h"
#import "NarikoTextField.h"
#import <APLSlideMenu/APLSlideMenuViewController.h>
#import "DownPicker.h"
#import "Company+CoreDataProperties.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface SurveyStartViewController () <NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet NarikoTextField *companyTextField;
@property (weak, nonatomic) IBOutlet NarikoTextField *addressTextField;
@property (weak, nonatomic) IBOutlet NarikoTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet NarikoTextField *postCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *sbiCodeTextField;
@property (assign, nonatomic) BOOL approved;

@property (strong, nonatomic) NSMutableArray* bandArray;
@property (strong, nonatomic) DownPicker *downPicker;
@property (assign, nonatomic) BOOL alreadyOpen;


- (IBAction)nextButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *beginSurvey;

@end

@implementation SurveyStartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getNavigationControllerSettings];
    
    if (self.statusOfPreview == YES)
    {
        self.beginSurvey.hidden = NO;
    }
    else
    {
        self.beginSurvey.hidden = YES;
    }
    
    self.companyTextField.placeHolderLabel.text  = @"Bedrijfsnaam :";
    self.addressTextField.placeHolderLabel.text  = @"Adres :";
    self.phoneTextField.placeHolderLabel.text    = @"Telefoonnummer :";
    self.postCodeTextField.placeHolderLabel.text = @"Postcode :";

    [self downPickerSettings];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

-(void) getNavigationControllerSettings
{
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"backForNavBar.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"quit-white-button"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 , 22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(void) downPickerSettings
{
    NSArray * resultArray = [SbiCode MR_findAllSortedBy:@"codeID" ascending:YES];
    self.bandArray        = [[NSMutableArray alloc] init];
    
    for (SbiCode * code in resultArray)
    {
        NSString * obj = code.codeID;
        [self.bandArray addObject:obj];
    }
    
    self.downPicker = [[DownPicker alloc] initWithTextField:self.sbiCodeTextField withData:self.bandArray];
    
    [self.downPicker showArrowImage:NO];
    [self.downPicker setPlaceholder:@""];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)dismissKeyboard
{
    [self.companyTextField resignFirstResponder];
    [self.addressTextField resignFirstResponder];
    [self.phoneTextField resignFirstResponder];
    [self.postCodeTextField resignFirstResponder];
    [self.sbiCodeTextField resignFirstResponder];
}

-(IBAction)exitButton:(id)sender
{
    APLSlideMenuViewController * aplslide  = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
    [self presentViewController:aplslide animated:YES completion:nil];
}


- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveUpView:)
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveDownView:)
                                                 name:UITextFieldTextDidEndEditingNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidBeginEditingNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidEndEditingNotification
                                                  object:nil];
}

-(void) moveUpView:(NSNotification*)notification
{
    if(self.sbiCodeTextField == notification.object)
    {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{self.view.frame = CGRectOffset(self.view.frame, 0, -20);}
                         completion:^(BOOL finished) {}];
    }
}

-(void) moveDownView:(NSNotification*)notification
{
    if(self.sbiCodeTextField == notification.object)
    {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut
                         animations:^{self.view.frame = CGRectOffset(self.view.frame, 0, +20);}
                         completion:^(BOOL finished) {}];
    }
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}




- (IBAction)nextButton:(id)sender
{
    [self makeValidation];
    
    if(self.approved == YES) {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.label.text = NSLocalizedString(@"Saving data", @"HUD loading title");
        
        CurrentUser  * user   = [CurrentUser MR_findFirst];
        
        NSDictionary * params = @{@"token"     : user.userToken ,
                                  @"user_id"   : user.userID,
                                  @"company"   : self.companyTextField.text,
                                  @"phone"     : self.phoneTextField.text,
                                  @"address"   : self.addressTextField.text,
                                  @"postcode"  : self.postCodeTextField.text,
                                  @"sbi"       : self.sbiCodeTextField.text};
        
        [[NetworkService sharedService] createPreviewWithParams:params andBlock:^(id responseObject, NSError *error)
        {
            if([responseObject[@"success"] isEqual:@(1)])
            {
                Company * newCompany       = [Company MR_findFirstOrCreateByAttribute:@"companyID" withValue:[NSString stringWithFormat:@"%@",responseObject[@"company_id"]]];
                newCompany.companyName     = self.companyTextField.text;
                newCompany.companyAddress  = self.addressTextField.text;
                newCompany.companyPhone    = self.phoneTextField.text;
                newCompany.companyPostCode = self.postCodeTextField.text;
                newCompany.companySBI      = self.sbiCodeTextField.text;
                
                [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                TryDateViewController * try = [self.storyboard instantiateViewControllerWithIdentifier:@"TryDateViewController"];
                [self.navigationController pushViewController:try animated:YES];
            }
            else
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Foutmelding!"
                                                                               message:@"Alle velden moeten worden ingevuld"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action)
                                                {
                                                                                                }];
                
                [alert addAction:defaultAction];
               
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }
    else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Foutmelding!"
                                                                       message:@"Alle velden moeten worden ingevuld"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
                                        {
                                        }];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) makeValidation {
    NSArray *tfields = @[self.companyTextField,self.addressTextField,self.phoneTextField,self.postCodeTextField,self.sbiCodeTextField];
    for (UITextField * tf in tfields) {
        NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
        if(tf.text.length == 0 || [[tf.text stringByTrimmingCharactersInSet: set] length] == 0 || [tf.text isEqualToString:@"Kies SBI code:"]) {
            self.approved = NO;
        }
        else {
            self.approved = YES;
        }
    }
}

- (IBAction)beginSurvey:(id)sender
{
    UINavigationController * nav = [self.storyboard instantiateViewControllerWithIdentifier:@"startSurvey"];
    [self presentViewController:nav animated:YES completion:nil];
}
@end
