//
//  DetailsViewController.m
//  Greenpoints
//
//  Created by Developer on 31.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UIView *popoverView;
- (IBAction)okButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;

@property (strong, nonatomic) NSArray * infoArray;

@end

@implementation DetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
-(void) viewWillAppear:(BOOL)animated
{
    [self downloadArrayOfInfo];
    
    self.popoverView.layer.cornerRadius = 15;
    self.popoverView.clipsToBounds = YES;
    
    self.infoTextView.text = [self.infoArray objectAtIndex:self.number];
    
     self.infoTextView.contentOffset = CGPointMake(0, 0);
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) downloadArrayOfInfo
{
    self.infoArray = [[NSArray alloc] initWithObjects:@"Lastige woorden:\n\nVoedselketen?\nAlle stappen die het voedsel aflegt tussen producent en consument. Bijvoorbeeld: Boer – transportbedrijf – slager – transportbedrijf - consument.\n\nLoonwerker/ Loonbedrijf?\nEen loonbedrijf of loonwerkbedrijf is een bedrijf dat loonwerk verricht, dat wil zeggen voor een opdrachtgever op contractbasis bepaalde werkzaamheden uitvoert. Het beschikt over gespecialiseerde machines en vakmensen die tegen betaling werk uitvoeren.\n\nToeleverancier?\nBedrijf dat goederen levert aan ander bedrijf. Bijvoorbeeld veevoer aan een boer.\n\nDienstverlener?\nEen dienst leveren i.p.v. een product. Iets wat niet tastbaar is. Bijvoorbeeld advies geven over de productie.",
                      @"Lastige woorden:\n\nVeganistische producten?\nProducten vrij van dierlijke producten. Dus geen vlees, zuivel, eieren, honing of producten waar deze dierlijke producten in verwerkt zitten. Daarnaast ook geen verzorgingsproducten, kleding of andere materialen waar dierlijke producten in zijn verwerkt, of die op dieren zijn getest.\n\nBodemverbeteraar?\nBodemverbeteraars zijn stoffen die aan de bodem toegevoegd worden zodat de planten beter groeien en gezonder zijn. Een bodemverbeteraar verhoogt de bodemvruchtbaarheid.\n\nBulk producten?\nProducten die in grote hoeveelheden worden geproduceerd en verpakt.\n\nSchelpdieren?\nMossel, oester, etc.\n\nSchaaldieren?\nKrab, garnalen en (rivier)kreeft.\n\nNon-food producten?\nNon-food is de aanduiding voor producten die niet eetbaar of drinkbaar zijn. Het begrip komt uit het Engels en betekent letterlijk 'geen voedsel'. Voorbeelden van non-food producten zijn tijdschriften, waspoeder, en cosmetica-artikelen.\n\nReststromen?\nReststroom (afval) of restafval is dat gedeelte van de afvalstroom dat overblijft nadat alle bruikbare en recyclebare afvalstromen van de hoofdstroom zijn gescheiden.\n\nVeterinaire diensten?\nDierengeneeskunde, dierenarts.",
                      @"Lastige woorden:\n\nFte?\nFte, een afkorting van fulltime-equivalent, is een rekeneenheid waarmee de omvang van een dienstverband of de personeelssterkte kan worden uitgedrukt. Bijvoorbeeld: 1 fte is een volledige werkweek van 40 uur, 0,8 fte is een werkweek van 32 uur.\n\nSchelpdieren?\nMossel, oester, etc.\n\nSchaaldieren?\nKrab, garnalen en (rivier)kreeft.\n\n Veganistische producten?\nProducten vrij van dierlijke producten. Dus geen vlees, zuivel, eieren, honing of producten waar deze dierlijke producten in verwerkt zitten. Daarnaast ook geen verzorgingsproducten, kleding of andere materialen waar dierlijke producten in zijn verwerkt, of die op dieren zijn getest.",
                      @"\n\nLastige woorden:\n\nGangbaar?\nWat veel voorkomt of gebruikelijk is.\n\nBiologisch?\nDe term biologisch heeft betrekking op de landbouwmethode. Oftewel de manier waarop ons voedsel tot stand gekomen is. Aan de biologische landbouw en veehouderij zijn strenge regels gesteld m.b.t. het milieu, de natuur en dierenwelzijn. Regels waaraan iedereen die biologische voeding produceert, zich moet houden.",nil];
}
- (IBAction)okButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end


//Lastige woorden vraag 1:
//
//Voedselketen?
//Alle stappen die het voedsel aflegt tussen producent en consument. Bijvoorbeeld: Boer – transportbedrijf – slager – transportbedrijf - consument.
//
//Loonwerker/ Loonbedrijf?
//Een loonbedrijf of loonwerkbedrijf is een bedrijf dat loonwerk verricht, dat wil zeggen voor een opdrachtgever op contractbasis bepaalde werkzaamheden uitvoert. Het beschikt over gespecialiseerde machines en vakmensen die tegen betaling werk uitvoeren.
//
//Toeleverancier?
//Bedrijf dat goederen levert aan ander bedrijf. Bijvoorbeeld veevoer aan een boer.
//
//Dienstverlener?
//Een dienst leveren i.p.v. een product. Iets wat niet tastbaar is. Bijvoorbeeld advies geven over de productie.
//
//Lastige woorden vraag 2:
//
//Veganistische producten?
//Producten vrij van dierlijke producten. Dus geen vlees, zuivel, eieren, honing of producten waar deze dierlijke producten in verwerkt zitten. Daarnaast ook geen verzorgingsproducten, kleding of andere materialen waar dierlijke producten in zijn verwerkt, of die op dieren zijn getest.
//
//Bodemverbeteraar?
//Bodemverbeteraars zijn stoffen die aan de bodem toegevoegd worden zodat de planten beter groeien en gezonder zijn. Een bodemverbeteraar verhoogt de bodemvruchtbaarheid.
//
//Bulk producten?
//Producten die in grote hoeveelheden worden geproduceerd en verpakt.
//
//Schelpdieren?
//Mossel, oester, etc.
//
//Schaaldieren?
//Krab, garnalen en (rivier)kreeft.
//
//Non-food producten?
//Non-food is de aanduiding voor producten die niet eetbaar of drinkbaar zijn. Het begrip komt uit het Engels en betekent letterlijk "geen voedsel". Voorbeelden van non-food producten zijn tijdschriften, waspoeder, en cosmetica-artikelen.
//
//Reststromen?
//Reststroom (afval) of restafval is dat gedeelte van de afvalstroom dat overblijft nadat alle bruikbare en recyclebare afvalstromen van de hoofdstroom zijn gescheiden.
//
//Veterinaire diensten?
//Dierengeneeskunde, dierenarts.
//
//Lastige woorden vraag 3:
//
//Fte?
//Fte, een afkorting van fulltime-equivalent, is een rekeneenheid waarmee de omvang van een dienstverband of de personeelssterkte kan worden uitgedrukt. Bijvoorbeeld: 1 fte is een volledige werkweek van 40 uur, 0,8 fte is een werkweek van 32 uur.
//
//Schelpdieren?
//Mossel, oester, etc.
//
//Schaaldieren?
//Krab, garnalen en (rivier)kreeft.
//
//Veganistische producten?
//Producten vrij van dierlijke producten. Dus geen vlees, zuivel, eieren, honing of producten waar deze dierlijke producten in verwerkt zitten. Daarnaast ook geen verzorgingsproducten, kleding of andere materialen waar dierlijke producten in zijn verwerkt, of die op dieren zijn getest.
//
//Lastige woorden vraag 8:
//
//Gangbaar?
//Wat veel voorkomt of gebruikelijk is.
//
//Biologisch?
//De term biologisch heeft betrekking op de landbouwmethode. Oftewel de manier waarop ons voedsel tot stand gekomen is. Aan de biologische landbouw en veehouderij zijn strenge regels gesteld m.b.t. het milieu, de natuur en dierenwelzijn. Regels waaraan iedereen die biologische voeding produceert, zich moet houden.

