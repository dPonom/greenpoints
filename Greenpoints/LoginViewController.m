//
//  ViewController.m
//  Greenpoints
//
//  Created by Developer on 16.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "LoginViewController.h"
#import "NetworkService.h"
#import "AFNetworking/AFNetworking.h"
#import "APLSlideMenuViewController.h"
#import "RegistrationViewController.h"
#import <MagicalRecord/MagicalRecord.h>
#import "CurrentUser+CoreDataProperties.h"

@interface LoginViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *logintextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions

- (IBAction)enterButton:(id)sender
{
    [[NetworkService sharedService] loginOfCurrenUserWithParams:@{@"login":self.logintextField.text,@"password":self.passwordTextField.text}
                                                       andBlock:^(id responseObject, NSError *error)
     {
         if (responseObject[@"error"])
         {
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                            message:@"Login or Password are wrong. Please, try again"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {}];
             
             [alert addAction:defaultAction];
             [self presentViewController:alert animated:YES completion:nil];
             
            
         }
         else
         {
             NSInteger result = [responseObject[@"education_type"] integerValue];
             NSString * myResult = [[NSString alloc] init];
             
             switch (result)
             {
                 case 1:
                        myResult = @"VO";
                     break;
                 case 2:
                        myResult = @"HBO";
                     break;
                 case 3:
                        myResult = @"MBO(1+2)";
                     break;
                 case 4:
                        myResult = @"MBO(3+4)";
                     break;
                     
                 default:
                     break;
             }
             
             NSString * userID = [NSString stringWithFormat:@"%@",responseObject[@"user_id"]];
             
             CurrentUser * user      = [CurrentUser MR_findFirstOrCreateByAttribute:@"userID" withValue:userID];
             user.userToken          = responseObject[@"token"];
             user.userEmail          = responseObject[@"email"];
             user.userEducationType  = myResult;
             user.userProfilePicture = responseObject[@"profile_pic"];
             
             if([responseObject[@"fullname"] isEqual:[NSNull null]] || [responseObject[@"school"] isEqual:[NSNull null]])
             {
                 user.userFullName       = @"empty";
                 user.userSchool         = @"empty";
             }
             else
             {
                 user.userFullName       = responseObject[@"fullname"];
                 user.userSchool         = responseObject[@"school"];
                 
             }
             
             [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
             
             APLSlideMenuViewController * slideMenu = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
             [self presentViewController:slideMenu animated:YES completion:nil];
         }
     }];
    
    
}
- (IBAction)registrationButton:(id)sender
{
    RegistrationViewController *registration = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
    [self presentViewController:registration animated:YES completion:nil];
}

@end
