//
//  DetailSurveyViewController.m
//  Greenpoints
//
//  Created by Developer on 22.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "DetailSurveyViewController.h"
#import "YoutubeViewController.h"
#import "DetailSurveyCell.h"
#import "NetworkService.h"
#import "Singleton.h"
#import "SurveyModel.h"
#import <MagicalRecord/MagicalRecord.h>
#import "CurrentUser+CoreDataProperties.h"

@interface DetailSurveyViewController () < UITableViewDataSource, UITableViewDelegate >

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSString * question;
@property (strong, nonatomic) NSString * answer;
@property (strong, nonatomic) NSString * youTubeLink;
@property (strong, nonatomic) NSMutableArray * currentSurvey;
@property (strong, nonatomic) NSMutableArray * sortedArray;
@property (strong, nonatomic) NSMutableArray * additionalSortedArray;

@end

@implementation DetailSurveyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentSurvey = [[NSMutableArray alloc] init];
    self.sortedArray = [[NSMutableArray alloc] init];
    self.additionalSortedArray = [[NSMutableArray alloc] init];
    
    [self getAnswersOfCurrentSurvey];
    
    
    UIFont * font = [[UIFont alloc] init];
   
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Mijn vragenlijsten"];
    [self.navigationController.navigationBar setTitleTextAttributes: @{ NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                                   NSFontAttributeName:font                 }];
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:nil
                                                                     action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"youTube2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 40, 40 );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(youTubeButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)youTubeButton:(id)sender
{
//    YoutubeViewController * settings = [self.storyboard instantiateViewControllerWithIdentifier:@"YoutubeViewController"];
//    settings.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//    settings.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    settings.company_id = self.company_id;
//    [self presentViewController:settings animated:YES completion:nil];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"YouTube"
                                                                              message: @"You can write or paste in text field link of your video"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        //textField.placeholder = @"name";
        textField.textColor       = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle     = UITextBorderStyleNone;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
    {
        [alertController dismissViewControllerAnimated:YES completion:^{}];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Upload" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        NSArray * textfields    = alertController.textFields;
        UITextField * linkField = textfields[0];
        self.youTubeLink        = linkField.text;
        [self downloadLinknYouTube];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ([DetailSurveyCell heightForText:[[self.additionalSortedArray objectAtIndex:indexPath.row] question]] + [DetailSurveyCell heightForText:[[self.additionalSortedArray objectAtIndex:indexPath.row] answer]])+27;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.additionalSortedArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailSurveyCell * cell = [tableView dequeueReusableCellWithIdentifier:@"detailSurvey"];
    
    cell.questionLabel.text = [[self.additionalSortedArray objectAtIndex:indexPath.row] question];
    cell.answerLabel.text = [[self.additionalSortedArray objectAtIndex:indexPath.row] answer];
    
    return cell;
}

-(void) getAnswersOfCurrentSurvey
{
    CurrentUser * user = [CurrentUser MR_findFirst];
    
    NSDictionary * params = @{@"user_id"    : user.userID,
                              @"token"      : user.userToken,
                              @"company_id" : self.company_id };
    
    [[NetworkService sharedService] getAnswersOfCurrenSurveyWithParams:params
                                                              andBlock:^(id responseObject, NSError *error)
    {
        NSArray * array = [responseObject objectForKey:@"answers"];
        for (id obj in array)
        {
            SurveyModel * survey = [[SurveyModel alloc] init];
            survey.question = [obj objectForKey:@"question"];
            survey.answer = [obj objectForKey:@"answer"];
            [self.currentSurvey addObject:survey];
        }
        [self sortingOfExistingArray];
        
        [self.tableView reloadData];
    }];
}

-(void) sortingOfExistingArray
{
    for (SurveyModel * obj in self.currentSurvey)
    {
        SurveyModel * newModel = [[SurveyModel alloc] init];
        for (SurveyModel * obj2 in self.currentSurvey)
        {
            if ([obj.question isEqualToString:obj2.question]&&[obj.answer isEqualToString:obj2.answer])
            {
                newModel.question = obj.question ;
                newModel.answer = obj.answer;
            }
            else if ([obj.question isEqualToString:obj2.question]&&![obj.answer isEqualToString:obj2.answer])
            {
                newModel.answer = [NSString stringWithFormat:@"%@,%@",newModel.answer,obj2.answer];
                
            }
        }
    [self.sortedArray addObject:newModel];
    }

    NSMutableSet *tempValues = [[NSMutableSet alloc] init];
    NSMutableArray *ret = [NSMutableArray array];
    for(SurveyModel * obj in self.sortedArray)
    {
        if(! [tempValues containsObject:[obj valueForKey:@"question"]]) {
            [tempValues addObject:[obj valueForKey:@"question"]];
            [ret addObject:obj];
        }
    }
    
    [self.additionalSortedArray addObjectsFromArray:ret];
}

#pragma mark DownloadYouTubeLink

-(void) downloadLinknYouTube
{
    CurrentUser * user = [CurrentUser MR_findFirst];
    
    NSDictionary * params = @{@"user_id"    : user.userID,
                              @"token"      : user.userToken,
                              @"company_id" : self.company_id,
                              @"link"       : self.youTubeLink };
    
    [[NetworkService sharedService] downloadLinkOnYouTubeWithParams:params andBlock:^(id responseObject, NSError *error)
     {
         if ([responseObject[@"success"] isEqual:@(1)])
         {
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Congratulations"
                                                                            message:@"your link is successfully uploaded"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                             {
                                                 [self dismissViewControllerAnimated:YES completion:nil];
                                             }];
             
             [alert addAction:defaultAction];
             [self presentViewController:alert animated:YES completion:nil];
             
         }
         else
         {
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                            message:@"something wrong"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {}];
             
             [alert addAction:defaultAction];
             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}

@end
