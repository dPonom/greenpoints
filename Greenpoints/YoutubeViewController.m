//
//  YoutubeViewController.m
//  Greenpoints
//
//  Created by Developer on 23.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "YoutubeViewController.h"
#import "Singleton.h"
#import "NetworkService.h"

@interface YoutubeViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *linkTextField;
@property (weak, nonatomic) IBOutlet UILabel *addYourLinkLabel;

@end

@implementation YoutubeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)downloadButton:(id)sender
{
    [self downloadLinknYouTube];
  
}

-(void) downloadLinknYouTube
{
    Singleton * sing = [Singleton sharedManager];
    
    NSDictionary * params = @{@"user_id"   : sing.user_id,
                              @"token"     : sing.token,
                              @"company_id": self.company_id,
                              @"link":@"sdfasdfasdfasf"};
    [[NetworkService sharedService] downloadLinkOnYouTubeWithParams:params andBlock:^(id responseObject, NSError *error)
     {
         if ([responseObject[@"success"] isEqual:@(1)])
         {
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Congratulations"
                                                                            message:@"your link is successfully uploaded"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action)
                                                                    {
                                                                     [self dismissViewControllerAnimated:YES completion:nil];
                                                                   }];
             
             [alert addAction:defaultAction];
             [self presentViewController:alert animated:YES completion:nil];

         }
         else
         {
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                            message:@"something wrong"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {}];
             
             [alert addAction:defaultAction];
             [self presentViewController:alert animated:YES completion:nil];
         }
     }];
}
//your link is successfully uploaded
@end
