//
//  FiveQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 23.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "FiveQuestionViewController.h"
#import "SixQuestionViewController.h"
#import "PopoverViewController.h"
#import "AdditionalFiveViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface FiveQuestionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *mainQuestionLabel;
@property (weak, nonatomic) IBOutlet UILabel *whereLabel;
@property (weak, nonatomic) IBOutlet UITextField *whereTextField;
- (IBAction)dontKnowButton:(id)sender;

@property (assign,nonatomic) BOOL variant;
@property (weak, nonatomic) IBOutlet UIButton *dontKnowButton;

- (IBAction)nextButton:(id)sender;
- (IBAction)infoButton:(id)sender;

@end

@implementation FiveQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.variant = NO;
    
    [self.whereLabel setTextColor:[UIColor whiteColor]];
    [self.dontKnowButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

}

- (IBAction)dontKnowButton:(id)sender
{
    [self.whereTextField resignFirstResponder];
    [self.whereLabel setTextColor:[UIColor blackColor]];
    [self.dontKnowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.variant = YES;
    self.whereTextField.text = @"";

}

- (IBAction)nextButton:(id)sender
{
    NSString * answer;
    if (self.variant == YES)
    {
        answer = @"Weet ik niet";
    }
    else
    {
        answer = self.whereTextField.text;
    }
    
    NSDictionary * dict = @{@"question":@"5. Waar gaan uw goederen uiteindelijk heen?",
                            @"answer":@[answer]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:4];
    
    AdditionalFiveViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AdditionalFiveViewController"];
    
    if (self.variant == YES)
    {
        vc.number = 0;
    }
    else
    {
        vc.number = 1;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 3;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
