//
//  RegistrationViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "RegistrationViewController.h"
#import "APLSlideMenuViewController.h"
#import "LoginViewController.h"
#import "NetworkService.h"
#import <MagicalRecord/MagicalRecord.h>
#import "CurrentUser+CoreDataProperties.h"
#import <MBProgressHUD/MBProgressHUD.h>


@interface RegistrationViewController () <UIPickerViewDelegate, UIPickerViewDataSource, UIToolbarDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong,nonatomic) NSArray * educationTypes;
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *repeatPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *educationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UITextField *educationTextField;
@property (weak, nonatomic) IBOutlet UIImageView *backEducationView;

@property (weak, nonatomic) IBOutlet UIImageView *confirmImageView;
@property (strong, nonatomic ) NSString * choosenEducationType;
@property (weak, nonatomic) IBOutlet UIToolbar *customToolBar;

@end

@implementation RegistrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.educationTypes = [[NSArray alloc] initWithObjects:@"HBO",@"MBO(3+4)",@"MBO(1+2)",@"VO", nil];
    
    self.pickerView.hidden = YES;
    self.confirmImageView.hidden = YES;
    self.customToolBar.hidden = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(self.repeatPasswordTextField.isEditing)
    {
        self.confirmImageView.hidden = NO;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if([self.passwordTextField.text isEqualToString:self.repeatPasswordTextField.text])
    {
       
        self.confirmImageView.image = [UIImage imageNamed:@"ok.png"];
    }
    else
    {
        
        self.confirmImageView.image = [UIImage imageNamed:@"notOk.png"];
    }
}

#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.educationTypes.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString * string = [[NSString alloc] init];
        
    string = [self.educationTypes objectAtIndex:row];
    
    return string;
}

- (IBAction)doneButtonAction:(id)sender
{
    NSInteger row = [self.pickerView selectedRowInComponent:0];
    self.educationLabel.text = [self.educationTypes objectAtIndex:row];
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.pickerView.alpha = 0;
                         self.customToolBar.alpha = 0;
                         self.educationTextField.hidden = NO;
                         self.backEducationView.hidden = NO;
                         
                     }
                     completion:^(BOOL finished){
                         self.pickerView.hidden = YES;
                         self.customToolBar.hidden = YES;
                         
                     }];
}

#pragma mark Actions

- (IBAction)educationButton:(id)sender
{
    [self.loginTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.repeatPasswordTextField resignFirstResponder];
    [self.fullNameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.educationTextField resignFirstResponder];
    
    self.educationTextField.hidden = YES;
    self.backEducationView.hidden = YES;
    self.pickerView.hidden = NO;
    self.customToolBar.hidden = NO;
    
    
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.pickerView.alpha = 1;
                         self.customToolBar.alpha = 1;
                     }
                     completion:nil];
}

- (IBAction)nextButton:(id)sender
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = NSLocalizedString(@"Registration...", @"HUD loading title");
    
    NSInteger  result;
    if ([self.educationLabel.text isEqualToString:@"HBO"])
    {
        result = 2;
    }
    
    else if ([self.educationLabel.text isEqualToString:@"MBO(3+4)"])
    {
        result = 4;
    }
    
    else if ([self.educationLabel.text isEqualToString:@"MBO(1+2)"])
    {
        result = 3;
    }
    
    else
    {
        result = 1;
    }
    
    NSString * string = [NSString stringWithFormat:@"%lu",(long)result ];
    
   
    NSDictionary * params = @{@"login"          : self.loginTextField.text,
                              @"password"       : self.passwordTextField.text,
                              @"email"          : self.emailTextField.text,
                              @"education_type" : string,//self.choosenEducationType
                              @"fullname"       : self.fullNameTextField.text,
                              @"school"         : self.educationTextField.text};
    
    [[NetworkService sharedService] registrationOfNewUserWithParams:params andBlock:^(id responseObject, NSError *error)
    {
         if([responseObject[@"success"] isEqual:@1])
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self loginOfCurrentUser];
        }
         else
        {
            NSLog(@"Error of registration");
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }];
    
}



- (IBAction)backToLoginButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void) loginOfCurrentUser
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = NSLocalizedString(@"Log In...", @"HUD loading title");
    
    
    [[NetworkService sharedService] loginOfCurrenUserWithParams:@{@"login"    : self.loginTextField.text,
                                                                  @"password" : self.passwordTextField.text}
                                                       andBlock:^(id responseObject, NSError *error)
     {
          if ([responseObject[@"success"] isEqual:@1])
         {
             NSString * userID = [NSString stringWithFormat:@"%@",responseObject[@"user_id"]];
             
             CurrentUser * user     = [CurrentUser MR_findFirstOrCreateByAttribute:@"userID" withValue:userID];
             user.userToken         = responseObject[@"token"];
             user.userEmail         = self.emailTextField.text;
             user.userFullName      = self.fullNameTextField.text;
             user.userEducationType = self.educationLabel.text;
             user.userSchool        = self.educationTextField.text;
             
             [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
             
             [MBProgressHUD showHUDAddedTo:self.view animated:YES];
             
             [self goToMainMapPage];
             
         }
          else
         {
             NSLog(@"Error of login");
             [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         }
     }];
}

-(void) goToMainMapPage
{
    APLSlideMenuViewController * slideMenu = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
    [self presentViewController:slideMenu animated:YES completion:nil];
}

@end
