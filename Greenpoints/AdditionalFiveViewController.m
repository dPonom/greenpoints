//
//  AdditionalFiveViewController.m
//  Greenpoints
//
//  Created by Dmitriy Ponomarenko on 20.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "AdditionalFiveViewController.h"
#import "SixQuestionViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface AdditionalFiveViewController ()

@property (weak, nonatomic) IBOutlet UILabel *variableQuestionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *yesChekmark;
@property (weak, nonatomic) IBOutlet UIImageView *noChekmark;

@property (assign ,nonatomic) BOOL answer;

@end

@implementation AdditionalFiveViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.number == 1)
    {
        self.variableQuestionLabel.text = @"5.3 Zou u willen dat het ergens anders heen gaat?";

    }
    else
    {
        self.variableQuestionLabel.text = @"5.3 Zou u willen weten waar het uiteindelijk heen gaat?";
    }
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)yesButton:(id)sender
{
    self.answer = YES;
    [self.yesChekmark setImage:[UIImage imageNamed:@"Checked.png"]];
    [self.noChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
}

- (IBAction)noButton:(id)sender
{
    self.answer = NO;
    [self.yesChekmark setImage:[UIImage imageNamed:@"notCheked.png"]];
    [self.noChekmark setImage:[UIImage imageNamed:@"Checked.png"]];
}

- (IBAction)nextButton:(id)sender
{
    NSString * answer;
    if(self.answer == YES)
    {
        answer = @"Ja";
    }
    else
    {
        answer = @"Nee";
    }
    NSDictionary * dict = @{@"question":self.variableQuestionLabel.text,
                            @"answer":@[answer]};
     [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:5];
    
    SixQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SixQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
