//
//  EndSurveyViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "EndSurveyViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"
#import "Preview.h"
#import "NetworkService.h"
#import "CurrentUser+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Company+Saving.h"

@interface EndSurveyViewController ()

@property (weak, nonatomic) IBOutlet UIButton *homeButton;
- (IBAction)homeButton:(id)sender;

@end

@implementation EndSurveyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Singleton * sing = [Singleton sharedManager];
    Company * comp = [[Company MR_findAll] lastObject];
    
    CurrentUser * user = [CurrentUser MR_findFirst];
    
    NSDictionary * params = @{@"user_id"    : user.userID,
                              @"token"      : user.userToken,
                              @"company_id" : comp.companyID,
                              @"survey_id"  : @"1",
                              @"answers"    : sing.answers};
    
    [[NetworkService sharedService] saveCurrentSurveyWithParams:params andBlock:^(id responseObject, NSError *error)
    {
            if([responseObject[@"success"] isEqual:@(1)])
            {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Success!"
                                                                               message:@"Your survey has been created"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action){}];
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        
            else
            
            {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error!"
                                                                               message:@"Your survey was not created"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action){}];
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)homeButton:(id)sender
{
    APLSlideMenuViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}
@end


