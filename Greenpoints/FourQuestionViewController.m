//
//  FourQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 23.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "FourQuestionViewController.h"
#import "FiveQuestionViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface FourQuestionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) NSString * string;
- (IBAction)nextButton:(id)sender;


@end

@implementation FourQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    self.descriptionTextView.text = @"Uw antwoord...";
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(!self.string)
    {
        self.descriptionTextView.text = @"";
    }
    else
    {
        self.descriptionTextView.text = self.string;
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.string = self.descriptionTextView.text;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}



- (IBAction)nextButton:(id)sender
{
    NSDictionary * dict = @{@"question":@"4. Aan wie verkoopt u? Wie zijn uw klanten?",
                            @"answer":@[self.descriptionTextView.text]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:3];
    
    FiveQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FiveQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}







@end
