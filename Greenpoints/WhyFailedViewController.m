//
//  WhyFailedViewController.m
//  Greenpoints
//
//  Created by павев on 05.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "WhyFailedViewController.h"
#import "SurveyStartViewController.h"
#import "CurrentUser+Saving.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Company+Saving.m"
#import "NetworkService.h"

@interface WhyFailedViewController ()

@property (weak, nonatomic) IBOutlet UITextField *whatTheySayTextField;
@property (weak, nonatomic) IBOutlet UITextField *workSheludeTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath * selectionIndexPath;

@property (strong, nonatomic) NSString * answer;

@property (strong, nonatomic) NSArray * answersVariants;

@end

@implementation WhyFailedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.answersVariants = @[@"Bedrijf wil niet meewerken",
                             @"Maar heeft nu geen tijd",
                             @"Telefoon werd niet opgenomen",
                             @"Bedrijf is geen food gerelateerd bedrijf",
                             @"Anders"];
    self.tableView.scrollEnabled = NO;
    [self.tableView setEditing:YES];
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.tableView.frame.size.height / 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.answersVariants.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"customCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [self.answersVariants objectAtIndex:indexPath.row];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.selectionIndexPath.row != indexPath.row)
    {
        [self.tableView deselectRowAtIndexPath:self.selectionIndexPath animated:NO];
    }
    
    self.selectionIndexPath = indexPath;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableViewStyle];
}

- (UITableViewCellEditingStyle)tableViewStyle
{
    return 3;
}

#pragma mark NextButtonAction

- (IBAction)nextButton:(id)sender
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if(self.whatTheySayTextField.text.length == 0 || [[self.whatTheySayTextField.text stringByTrimmingCharactersInSet: set] length] == 0 ||
       self.workSheludeTextField.text.length == 0 || [[self.workSheludeTextField.text stringByTrimmingCharactersInSet: set] length] == 0 || !self.selectionIndexPath)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Foutmelding!"
                                                                       message:@"Alle velden moeten worden ingevuld"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
                                        {
                                        }];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert animated:YES completion:nil];;
    }
    else
    {
        self.answer = [self.answersVariants objectAtIndex:self.selectionIndexPath.row];
        NSString * fullAnser = [NSString stringWithFormat:@"Ja, en niet gelukt  -  %@,Gesproken met - %@,Functie in bedrijf - %@", self.answer,self.whatTheySayTextField.text,
                                self.workSheludeTextField.text];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.label.text = NSLocalizedString(@"Saving Preview", @"HUD loading title");
        
        CurrentUser * user = [CurrentUser MR_findFirst];
        Company * company  = [[Company MR_findAll] lastObject];
        
        NSDictionary * params = @{@"user_id"       : user.userID,
                                  @"token"         : user.userToken,
                                  @"company_id"    : company.companyID,
                                  @"survey_status" : fullAnser };
        [[NetworkService sharedService] sendPreviewResult:params andBlock:^(id responseObject, NSError *error)
         {
             if([responseObject[@"success"] isEqual:@1])
             {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 UINavigationController * nav = [self.storyboard instantiateViewControllerWithIdentifier:@"startSurvey"];
                 [self presentViewController:nav animated:YES completion:nil];
             }
             else
             {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 NSLog(@"error of sending preview to server");
             }
         }];
    }
}

@end
