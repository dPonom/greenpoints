//
//  SecondQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 22.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SecondQuestionViewController.h"
#import "CustomCell.h"
#import "ThirdQuestionViewController.h"
#import "DetailsViewController.h"
#import "PopoverViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface SecondQuestionViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *answers;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *detailsTextField;
@property (weak, nonatomic) IBOutlet UILabel *multipleAnswersLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;

@property (strong, nonatomic) NSMutableArray * titlesOfRows;

@property (strong, nonatomic) NSMutableArray * arrayOfArrays;
@property (strong, nonatomic) NSMutableArray * array1;
@property (strong, nonatomic) NSMutableArray * array2;
@property (strong, nonatomic) NSMutableArray * array3;
@property (strong, nonatomic) NSMutableArray * array4;
@property (strong, nonatomic) NSMutableArray * array5;
@property (strong, nonatomic) NSMutableArray * array6;
@property (strong, nonatomic) NSMutableArray * array7;
@property (strong, nonatomic) NSMutableArray * array8;
@property (strong, nonatomic) NSMutableArray * array9;
@property (strong, nonatomic) NSMutableArray * array10;

@property (strong, nonatomic) NSString * currentAnswer;
@property (strong, nonatomic) NSIndexPath * latestSelectedIndexPath;


- (IBAction)nextButton:(id)sender;
- (IBAction)infoButton:(id)sender;
- (IBAction)questionButton:(id)sender;

@end

@implementation SecondQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.titlesOfRows = [[NSMutableArray alloc] init];
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self initArrayForAnswers];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"backForNavBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.tableView.editing = YES;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    return [self.multipleAnswers count];
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString * sectionName = [[NSString alloc] init];
    //self.tableView
    
    //[sectionName setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:18.0]];
    NSInteger  number = [[self.multipleAnswers objectAtIndex:section] integerValue];

    switch (number)
    {
        case 0:
            sectionName = @"Primaire producent";
            return sectionName;
            break;
        case 1:
            sectionName = @"Verwerker";
            return sectionName;
            break;
        case 2:
            sectionName = @"Transporteur";
            return sectionName;
            break;
        case 3:
            sectionName = @"Groothandel";
            return sectionName;
            break;
        case 4:
             sectionName = @"Instellingskeuken";
            return sectionName;
            break;
        case 5:
            sectionName = @"Detailhandel";
            return sectionName;
            break;
        case 6:
            sectionName = @"Loonwerker";
            return sectionName;
            break;
        case 7:
            sectionName = @"Toeleverancier";
            return sectionName;
            break;
        case 8:
            sectionName = @"Dienstverlener";
            return sectionName;
            break;
            
        default:
            sectionName = @"zijn versie";
            return sectionName;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CustomCell heightForText:[[self.arrayOfArrays objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.arrayOfArrays objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = [[self.arrayOfArrays objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:18.0]];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.latestSelectedIndexPath = indexPath;
    NSString * name =  [[self.arrayOfArrays objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
    [self.titlesOfRows addObject:name];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
    NSString * name=  [[self.arrayOfArrays objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self.titlesOfRows removeObject:name];
}

- (UITableViewCellEditingStyle)tableViewStyle
{
    return 3;
}

- (UITableViewCellEditingStyle)tableView:(UITableView * )tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableViewStyle];
}

#pragma mark Init Array

-(void) initArrayForAnswers
{
    [self initializationOfArrays];
    self.arrayOfArrays = [[NSMutableArray alloc] init];
    for (NSString * obj in self.multipleAnswers)
    {
        NSInteger  number = [obj integerValue];
        switch (number)
        {
            case 0:
                [self.arrayOfArrays addObject:self.array1];
                break;
            case 1:
                [self.arrayOfArrays addObject:self.array2];
                break;
            case 2:
                [self.arrayOfArrays addObject:self.array3];
                break;
            case 3:
                [self.arrayOfArrays addObject:self.array4];
                break;
            case 4:
                [self.arrayOfArrays addObject:self.array5];
                break;
            case 5:
                [self.arrayOfArrays addObject:self.array6];
                break;
            case 6:
                [self.arrayOfArrays addObject:self.array7];
                break;
            case 7:
                [self.arrayOfArrays addObject:self.array8];
                break;
            case 8:
                [self.arrayOfArrays addObject:self.array9];
                break;
                
            default:
                [self.arrayOfArrays addObject:self.array10];
                break;
        }
    }
}


-(void) initializationOfArrays
{

    self.array1 = [[NSMutableArray alloc] initWithObjects:@"koeien",
                              @"varkens",
                              @"paarden",
                              @"schapen",
                              @"geiten",
                              @"pluimvee",
                              @"zuivel",
                              @"eieren",
                              @"vlees",
                              @"vis, schaaldieren",
                              @"insecten, wormen",
                              @"groente",
                              @"fruit",
                              @"aardappelen",
                              @"bieten",
                              @"granen",
                              @"lupine",
                              @"snijmaïs",
                              @"anders",
                              nil];
    
    self.array2 = [[NSMutableArray alloc] initWithObjects:@"vlees en vleeswaren",
                              @"varkens",
                              @"zuivelproducten",
                              @"groenteproducten",
                              @"fruitproducten",
                              @"eierproducten",
                              @"vis en schaaldieren",
                              @"aardappelproducten",
                              @"bietenproducten",
                              @"graanproducten",
                              @"veganistische producten",
                              @"veevoer",
                              @"bodemverbeteraar",
                              @"hulpstoffen/grondstoffen voor industriële toepassingen",
                              @"bieten",
                              @"granen",
                              @"lupine",
                              @"snijmaïs",
                   @"anders",
                              nil];
    
    self.array3 = [[NSMutableArray alloc] initWithObjects:@"levende dieren",
                                                          @"bulkproducten",
                                                          @"gekoelde of bevroren producten",
                                                          @"opslagruimte",
                   @"anders",
                              nil];
    
    self.array4 = [[NSMutableArray alloc] initWithObjects:@"brood en banket",
                              @"vlees en vleeswaren",
                              @"groente en fruit",
                              @"vis en schaaldieren",
                              @"gerechten en maaltijden",
                              @"groente- en fruitsappen",
                              @"non-food producten",
                   @"anders",
                              nil];

    self.array5 = [[NSMutableArray alloc] initWithObjects:@"ontbijt",
                              @"lunch",
                              @"diner",
                              @"snacks",
                              @"high tea",
                              @"barbecue",
                              @"buffet",
                              @"catering feesten en recepties",
                              @"anders",
                              nil];
    
    self.array6 = [[NSMutableArray alloc] initWithObjects:@"brood en banket",
                              @"vlees en vleeswaren",
                              @"groente en fruit",
                              @"vis en schaaldieren",
                              @"insecten en wormen",
                              @"gerechten en maaltijden",
                              @"groente- en fruitsappen",
                              @"non-food producten",
                              @"anders",
                              nil];
   
    self.array7 = [[NSMutableArray alloc] initWithObjects:@"uitvoering machinaal landwerk",
                              @"diensten sensortechnologie",
                              @"stortgelegenheid reststromen",
                              @"anders",
                              nil];
    
    self.array8 = [[NSMutableArray alloc] initWithObjects:@"machines, mechanisering en automatisering",
                              @"veevoer",
                              @"meststoffen",
                              @"zaad-, plant- en pootgoed",
                              @"diergeneesmiddelen, gewasbeschermings- en ontsmettingsmiddelen",
                              @"anders",
                              nil];
    
    self.array9 = [[NSMutableArray alloc] initWithObjects:@"adviezen",
                              @"administratieve diensten",
                              @"controlediensten, verklaringen en certificaten",
                              @"veterinaire diensten",
                              @"andere dienstverlening",
                              nil];
    
    self.array10 = [[NSMutableArray alloc] initWithObjects:@"aub beneden het product / handelswaar / dienst invullen",nil];
}



#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.tableView deselectRowAtIndexPath:self.latestSelectedIndexPath animated:NO];
    return YES;
}

#pragma mark ActionButtons

- (IBAction)nextButton:(id)sender
{
    if(self.titlesOfRows.count > 0) {
        NSDictionary * dict = @{@"question":@"2. Wat verkoopt u?",
                                @"answer":self.titlesOfRows};
        [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:1];
        
        ThirdQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ThirdQuestionViewController"];

        vc.multipleAnswers = [[NSMutableArray alloc] init];
        [vc.multipleAnswers addObjectsFromArray:self.multipleAnswers];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Attentie!"
                                                                       message:@"U moet ten minste één antwoord kiezen"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
                                        {
                                        }];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 1;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)questionButton:(id)sender
{
    DetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 1;
    [self presentViewController:vc animated:YES completion:nil];
}


@end
