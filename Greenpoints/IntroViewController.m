//
//  IntroViewController.m
//  Greenpoints
//
//  Created by Dmitriy Ponomarenko on 10.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "IntroViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MagicalRecord/MagicalRecord.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "NetworkService.h"
#import <APLSlideMenu/APLSlideMenuViewController.h>
#import "CurrentUser+CoreDataProperties.h"

@interface IntroViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic ) CurrentUser * user;

@end

@implementation IntroViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.textView.text = @"      Welkom!\n      Met deze app brengen we de gehele voedselketen in kaart. Met jouw hulp hebben we straks in één oogopslag  alle ondernemers die iets met voedsel te maken hebben in beeld. In deze app kun je op de kaart zien waar de ondernemers zich bevinden en vind je de vragenlijst voor het afnemen van een interview.\n      Snap je de vraag of een bepaald woord niet helemaal, druk dan op het vraagteken (?).\n      Voor vragen over het gebruik van de app, druk bovenaan op de letter i  (i).\n       Heel veel succes!";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated
{
    self.user = [CurrentUser MR_findFirst];
    
    if(self.user.userID && self.user.userToken)
    {
        [self goToMainMapPage];
    }
}

-(void) goToMainMapPage
{
    APLSlideMenuViewController * slideMenu = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
    [self presentViewController:slideMenu animated:YES completion:nil];
}

@end
