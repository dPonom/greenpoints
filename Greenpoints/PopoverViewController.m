//
//  PopoverViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "PopoverViewController.h"

@interface PopoverViewController ()
@property (weak, nonatomic) IBOutlet UIButton *okButton;
- (IBAction)okButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *popoverView;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;

@property (strong,nonatomic) NSArray * infoArray;


@end

@implementation PopoverViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self downloadArrayOfInfo];
    
    self.popoverView.layer.cornerRadius = 15;
    self.popoverView.clipsToBounds = YES;
    
    self.infoTextView.text = [self.infoArray objectAtIndex:self.number];
    
    self.infoTextView.contentOffset = CGPointMake(0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)okButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) downloadArrayOfInfo
{
    self.infoArray = [[NSArray alloc] initWithObjects:@"     Intro:\n\n     Een ondernemer heeft een bepaalde rol in de voedselketen. Hij kan bijvoorbeeld voedsel produceren, verkopen, maar ook transporteren. Een ondernemer heeft niet altijd één rol, maar kan ook meerdere rollen tegelijk vervullen. In vraag 1 geef je aan welke rol de ondernemer heeft in de voedselketen. Meerdere antwoorden zijn mogelijk.",
    @"     Intro:\n\n     Om in kaart te brengen wat er geproduceerd wordt in de regio willen wij graag weten wat de ondernemers verkopen. Het kan hier bijvoorbeeld gaan om dieren, groenten, maar ook diensten. Meerdere antwoorden mogelijk.",
    @"     Intro:\n\n     Naast dat we willen weten wát er geproduceerd of verkocht wordt, willen we ook weten hoeveel. Dit is belangrijk voor een compleet beeld.",
    @"     Intro:\n\n     Soms zitten er vele schakels in een voedselketen en gaan de goederen van ondernemer naar ondernemer. Maar waar komen de goederen uiteindelijk terecht? Daar gaat deze vraag over.",
    @"     Intro:\n\n     Bij het produceren van goederen ontstaat vaak afval. Vaak is dit afval nog te recyclen. Kan dit niet, dan wordt het afval ook wel reststroom genoemd. In vraag 6 willen we weten welke reststromen de ondernemer  heeft.",
    @"     Intro:\n\n     Als ondernemer werk je vaak samen met anderen. Toch kan deze samenwerking er heel verschillend uitzien \n-	Wanneer is er sprake van samenwerking?\n Als geen van de partners zo maar vervangen kan worden. Er is dus een bepaalde band tussen de samenwerkingspartners, bijvoorbeeld omdat zij\n-	bij elkaar in de buurt zitten\n-	bepaalde uitgangspunten delen\n-	unieke producten hebben\n-	De samenwerking\n-	komt vrijwillig tot stand\n-	is altijd tussen zelfstandige ondernemers (dus bijvoorbeeld geen maatschap)\n-	en heeft altijd voor alle samenwerkende partners een meerwaarde.\n\nWe onderscheiden twee typen samenwerking:\n-	We spreken van horizontale samenwerking als je samenwerkt met ondernemers uit een gelijksoortig bedrijf (voorbeelden: een aantal melkveehouders leveren samen hun melk aan de zuivelfabriek; of een aantal appeltelers leveren samen hun fruit aan de sapfabriek).\n-	We spreken van verticale samenwerking als je samenwerkt met ondernemers uit een of meer andersoortige bedrijven (voorbeeld: een varkensfokker en een bakker leveren samen vlees en paneermeel aan een bedrijf dat kroketten maakt.) ",
    @"     Intro:\n\n     Er zijn verschillende productiemethoden die ondernemers kunnen toepassen. Voorbeelden zijn gangbaar en biologisch. De methodes verschillen van elkaar doordat er andere eisen worden gesteld en uiteindelijk vaak andere producten oplevert.",
    @"     Intro:\n\n     Een ondernemer streeft altijd doelen na met zijn onderneming. Een voorbeeld hiervan is winst maken. Toch zijn er steeds meer ondernemers die ook maatschappelijke doelen nastreven. Dit zijn doelen die niet alleen voordeel opleveren voor de ondernemer, maar ook voor de maatschappij. Oftewel, voor iedereen. Een voorbeeld hiervan is het gebruik van duurzame energie. Dit kan de ondernemer geld besparen, maar zorgt ook dat er minder vervuilende stoffen worden uitgestoten. Dit is positief voor iedereen. Andere voorbeelden zijn: koeien in de wei, biologisch produceren.",
    @"     Intro:\n\n     Unique selling points (‘usp’) zijn de sterke kanten van een product of bedrijf die het onderscheidt, oftewel anders maakt, van vergelijkbare producten of bedrijven. Met die punten laat je zien waarom je als ondernemer bijzonder bent en waarom anderen met je willen samenwerken",
    @"     Intro:\n\n     Een ondernemer is altijd in beweging en houdt de markt goed in de gaten. Zo moet de ondernemer blijven leren en past zich aan als dat nodig is.",
    @"     Intro:\n\n     Een ondernemer moet altijd vooruit kijken en het werkveld goed in de gaten houden. Mogelijk zijn er aanpassingen nodig om het in de toekomst nog beter te doen of om de zaak draaiende te houden\n- Hier kan bijvoorbeeld gedacht worden aan het bouwen van een nieuwe schuur, nieuwe gewassen produceren, meer personeel aannemen of stoppen met het bedrijf.",
    @"     Intro:\n\n     Elke onderneming betekent iets voor zijn stakeholders. Een stakeholder is een persoon of een partij die betrokken is bij de onderneming of bedrijfsproces. Voorbeelden zijn klanten, leveranciers of aandeelhouders. Met het leveren van een product of dienst voldoe je aan een bepaalde vraag bij de stakeholder en beteken je dus iets voor die persoon of onderneming. De volgende vraag gaat hier op in.",
    @"     Intro:\n\n     Waar staat een ondernemer voor en hoe wil hij/zij graag dat anderen met hem/haar omgaat?/nHoe moet een onderneming zich wel of niet gedragen? Voorbeelden zijn: afspraak is afspraak, op tijd betalen, dat je elkaar kan vertrouwen en dat je elkaar op de hoogte houdt als er iets veranderd.",
    @"     Intro:\n\n     Een onderneming heeft vaak een doel voor ogen en een bepaalde ambitie om iets te bereiken. Een ambitie kan bijvoorbeeld zijn: in twee jaar 10% meer winst, over 5 jaar een filiaal erbij of van 2 naar 4 gewassen.",
    @"     Intro:\n\n     Voor een onderneming is het belangrijk hoe  de omgeving en branche van het bedrijf eruit ziet. Een branche is een groep van bedrijven die samen actief zijn in een bepaalde groep goederen en diensten. Voorbeelden zijn: akkerbouw, transportbranche of melkveehouderij.",
    @"     Intro:\n\n     Hoe een onderneming vandaag de dag georganiseerd is betekent niet gegarandeerd succes in de toekomst. Ondernemers moeten zich aan vele omstandigheden aanpassen.",
    @"     Intro:\n\n     We doen dit interview met u en alle andere food-ondernemers in de regio om erachter te komen of we niet samen met grote regionale inkopers en u als food-producenten een keten binnen de regio kunnen ontwikkelen. We kijken wat de grote klanten, zoals bijvoorbeeld grote keukens en inkopers van grote instellingen nodig hebben en hoe alle ondernemers samen hiervoor een aanbod kunnen doen. Het zal nog even een beetje duren voordat alle informatie er is, maar daarna willen we met belangstellende ondernemers aan het werk om te kijken hoe ze zich kunnen specialiseren voor een plek in de voedselketen.",
    nil];
//Intro vraag 1:
//Een ondernemer heeft een bepaalde rol in de voedselketen. Hij kan bijvoorbeeld voedsel produceren, verkopen, maar ook transporteren. Een ondernemer heeft niet altijd één rol, maar kan ook meerdere rollen tegelijk vervullen. In vraag 1 geef je aan welke rol de ondernemer heeft in de voedselketen. Meerdere antwoorden zijn mogelijk.
//
//Intro vraag 2:
//Om in kaart te brengen wat er geproduceerd wordt in de regio willen wij graag weten wat de ondernemers verkopen. Het kan hier bijvoorbeeld gaan om dieren, groenten, maar ook diensten. Meerdere antwoorden mogelijk.
//
//Intro vraag 3:
//Naast dat we willen weten wát er geproduceerd of verkocht wordt, willen we ook weten hoeveel. Dit is belangrijk voor een compleet beeld.
//
//Intro vraag 4:
//
//Intro vraag 5:
//Soms zitten er vele schakels in een voedselketen en gaan de goederen van ondernemer naar ondernemer. Maar waar komen de goederen uiteindelijk terecht? Daar gaat deze vraag over.
//
//
//Intro vraag 6:
//Bij het produceren van goederen ontstaat vaak afval. Vaak is dit afval nog te recyclen. Kan dit niet, dan wordt het afval ook wel reststroom genoemd. In vraag 6 willen we weten welke reststromen de ondernemer  heeft.
//
//Intro vraag 7:
//Als ondernemer werk je vaak samen met anderen. Toch kan deze samenwerking er heel verschillend uitzien.
//-	Wanneer is er sprake van samenwerking?
//Als geen van de partners zo maar vervangen kan worden. Er is dus een bepaalde band tussen de samenwerkingspartners, bijvoorbeeld omdat zij
//-	bij elkaar in de buurt zitten
//-	bepaalde uitgangspunten delen
//-	unieke producten hebben
//-	De samenwerking
//-	komt vrijwillig tot stand
//-	is altijd tussen zelfstandige ondernemers (dus bijvoorbeeld geen maatschap)
//-	en heeft altijd voor alle samenwerkende partners een meerwaarde.
//
//We onderscheiden twee typen samenwerking:
//-	We spreken van horizontale samenwerking als je samenwerkt met ondernemers uit een gelijksoortig bedrijf (voorbeelden: een aantal melkveehouders leveren samen hun melk aan de zuivelfabriek; of een aantal appeltelers leveren samen hun fruit aan de sapfabriek).
//-	We spreken van verticale samenwerking als je samenwerkt met ondernemers uit een of meer andersoortige bedrijven (voorbeeld: een varkensfokker en een bakker leveren samen vlees en paneermeel aan een bedrijf dat kroketten maakt.)
//
//
//Intro vraag 8:
//
//Er zijn verschillende productiemethoden die ondernemers kunnen toepassen. Voorbeelden zijn gangbaar en biologisch. De methodes verschillen van elkaar doordat er andere eisen worden gesteld en uiteindelijk vaak andere producten oplevert.
//
//Introvraag 9:
//Een ondernemer streeft altijd doelen na met zijn onderneming. Een voorbeeld hiervan is winst maken. Toch zijn er steeds meer ondernemers die ook maatschappelijke doelen nastreven. Dit zijn doelen die niet alleen voordeel opleveren voor de ondernemer, maar ook voor de maatschappij. Oftewel, voor iedereen. Een voorbeeld hiervan is het gebruik van duurzame energie. Dit kan de ondernemer geld besparen, maar zorgt ook dat er minder vervuilende stoffen worden uitgestoten. Dit is positief voor iedereen. Andere voorbeelden zijn: koeien in de wei, biologisch produceren.
//
//Intro vraag 10:
//Unique selling points (‘usp’) zijn de sterke kanten van een product of bedrijf die het onderscheidt, oftewel anders maakt, van vergelijkbare producten of bedrijven. Met die punten laat je zien waarom je als ondernemer bijzonder bent en waarom anderen met je willen samenwerken
//Intro vraag 11:
//Een ondernemer is altijd in beweging en houdt de markt goed in de gaten. Zo moet de ondernemer blijven leren en past zich aan als dat nodig is.
//
//Intro vraag 12:
//Een ondernemer moet altijd vooruit kijken en het werkveld goed in de gaten houden. Mogelijk zijn er aanpassingen nodig om het in de toekomst nog beter te doen of om de zaak draaiende te houden- Hier kan bijvoorbeeld gedacht worden aan het bouwen van een nieuwe schuur, nieuwe gewassen produceren, meer personeel aannemen of stoppen met het bedrijf.
//
//Intro vraag 13:
//Elke onderneming betekent iets voor zijn stakeholders. Een stakeholder is een persoon of een partij die betrokken is bij de onderneming of bedrijfsproces. Voorbeelden zijn klanten, leveranciers of aandeelhouders. Met het leveren van een product of dienst voldoe je aan een bepaalde vraag bij de stakeholder en beteken je dus iets voor die persoon of onderneming. De volgende vraag gaat hier op in.
//
//Intro vraag 14:
//Waar staat een ondernemer voor en hoe wil hij/zij graag dat anderen met hem/haar omgaat?
//Hoe moet een onderneming zich wel of niet gedragen? Voorbeelden zijn: afspraak is afspraak, op tijd betalen, dat je elkaar kan vertrouwen en dat je elkaar op de hoogte houdt als er iets veranderd.
//
//Intro vraag 15:
//Een onderneming heeft vaak een doel voor ogen en een bepaalde ambitie om iets te bereiken. Een ambitie kan bijvoorbeeld zijn: in twee jaar 10% meer winst, over 5 jaar een filiaal erbij of van 2 naar 4 gewassen.
//
//Intro vraag 16:
//Voor een onderneming is het belangrijk hoe  de omgeving en branche van het bedrijf eruit ziet. Een branche is een groep van bedrijven die samen actief zijn in een bepaalde groep goederen en diensten. Voorbeelden zijn: akkerbouw, transportbranche of melkveehouderij.
//
//Intro vraag 17:
//Hoe een onderneming vandaag de dag georganiseerd is betekent niet gegarandeerd succes in de toekomst. Ondernemers moeten zich aan vele omstandigheden aanpassen.
//
//Intro vraag 20:
//We doen dit interview met u en alle andere food-ondernemers in de regio om erachter te komen of we niet samen met grote regionale inkopers en u als food-producenten een keten binnen de regio kunnen ontwikkelen. We kijken wat de grote klanten, zoals bijvoorbeeld grote keukens en inkopers van grote instellingen nodig hebben en hoe alle ondernemers samen hiervoor een aanbod kunnen doen. Het zal nog even een beetje duren voordat alle informatie er is, maar daarna willen we met belangstellende ondernemers aan het werk om te kijken hoe ze zich kunnen specialiseren voor een plek in de voedselketen.
}

@end

