//
//  WorkWithViewController.m
//  Greenpoints
//
//  Created by Developer on 16.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "APLMenuViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"
#import "NetworkService.h"
#import "ListOfSurveysViewController.h"



@interface APLMenuViewController ()

@property (strong,nonatomic ) NSMutableArray * companies;

@end

@implementation APLMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Menu"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"backForNavBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    //[self getListOfSurveysForCurrentUser];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)profileButton:(id)sender
{
    UINavigationController * nav = [self.storyboard instantiateViewControllerWithIdentifier:@"profile"];
    [self presentViewController:nav animated:YES completion:nil];
}

- (IBAction)mySurveysButton:(id)sender
{
    UINavigationController * nav = [self.storyboard instantiateViewControllerWithIdentifier:@"table"];
    //[[(ListOfSurveysViewController *)nav.viewControllers.firstObject companiesFromServer] setArray:self.companies];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}

- (IBAction)createNewSurvey:(id)sender
{
    UINavigationController * nav = [self.storyboard instantiateViewControllerWithIdentifier:@"newSurvey"];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}


//- (void) getListOfSurveysForCurrentUser
//{
//    self.companies = [[NSMutableArray alloc] init];
//    Singleton * sing = [Singleton sharedManager];
//    
//    NSDictionary * params = @{@"user_id" : sing.user_id,
//                              @"token"   : sing.token    };
//    [[NetworkService sharedService] getSurveysOfCurrentUserWithParams:params andBlock:^(id responseObject, NSError *error)
//     { NSInteger indexValue = 0;
//         for (Company * comp in responseObject[@"companies"])
//         {
//             comp.companyAddress = [[[responseObject objectForKey:@"companies"] objectAtIndex:indexValue] objectForKey:@"address"];
//             comp.companyName = [[[responseObject objectForKey:@"companies"]objectAtIndex:indexValue] objectForKey:@"company"];
//             comp.company_id = [[[responseObject objectForKey:@"companies"]objectAtIndex:indexValue] objectForKey:@"company_id"];
//             comp.phone = [[[responseObject objectForKey:@"companies"]objectAtIndex:indexValue] objectForKey:@"phone"];
//             comp.sbi = [[[responseObject objectForKey:@"companies"]objectAtIndex:indexValue] objectForKey:@"sbi"];
//             comp.youtube_link = [[[responseObject objectForKey:@"companies"]objectAtIndex:indexValue] objectForKey:@"youtube_link"];
//             [self.companies addObject:comp];
//             indexValue++;
//         }
//     }];
//}

    

@end
