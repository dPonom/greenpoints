//
//  QuestionsViewController.m
//  Greenpoints
//
//  Created by Developer on 19.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "QuestionsViewController.h"
#import "SecondQuestionViewController.h"
#import "CustomCell.h"
#import "PopoverViewController.h"
#import "DetailsViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface QuestionsViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate >
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *multipleAnswersLabel;
@property (strong, nonatomic) NSMutableArray *answers;
@property (strong, nonatomic) NSMutableArray * tempNumbersOfRows;
@property (strong, nonatomic) NSMutableArray * titlesOfRows;
- (IBAction)nextButton:(id)sender;
- (IBAction)infoButton:(id)sender;
- (IBAction)questionButton:(id)sender;


@end

@implementation QuestionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tempNumbersOfRows = [[NSMutableArray alloc] init];
    self.titlesOfRows = [[NSMutableArray alloc] init];
    
    
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self initArrayForAnswers];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"backForNavBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.tableView.editing = YES;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark UITableViewDataSource


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CustomCell heightForText:[self.answers objectAtIndex:indexPath.row]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.answers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UIColor * color = [UIColor colorWithRed:127.0/255.0 green:226.0/255.0 blue:144.0/255.0 alpha:1.0];
    CustomCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = [self.answers objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:18.0]];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
    NSString * number = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    NSString * name=  [self.answers objectAtIndex:indexPath.row];
    [self.titlesOfRows addObject:name];
    [self.tempNumbersOfRows addObject:number];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
    NSString * number = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    NSString * name=  [self.answers objectAtIndex:indexPath.row];
    [self.titlesOfRows removeObject:name];
    [self.tempNumbersOfRows removeObject:number];
}

- (UITableViewCellEditingStyle)tableViewStyle
{
    return 3;
}

- (UITableViewCellEditingStyle)tableView:(UITableView * )tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableViewStyle];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)nextButton:(id)sender
{
    if (self.tempNumbersOfRows.count > 0)
    {
        NSDictionary * dict = @{@"question":@"1. Wat is uw rol in de voedselketen?",
                                @"answer":self.titlesOfRows};
        [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:0];
        
        SecondQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondQuestionViewController"];
        NSLog(@"%@",self.tempNumbersOfRows);
        vc.multipleAnswers = [[NSMutableArray alloc] init];
        [vc.multipleAnswers addObjectsFromArray:self.tempNumbersOfRows];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Attentie!"
                                                                       message:@"U moet ten minste één antwoord kiezen"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action)
                                        {
                                        }];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 0;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)questionButton:(id)sender
{
    DetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 0;
    [self presentViewController:vc animated:YES completion:nil];
}


-(void) initArrayForAnswers
{
    self.answers = [[NSMutableArray alloc] initWithObjects:@"Primaire producent (boer, tuinder)",
                                                           @"Verwerker (ambachtelijk: slager, molenaar, kaasboerderij etc. of industrieel: AVEBE, zuivelfabriek, Suikerunie etc.)",
                                                           @"Transporteur (bulk, veetransport, koeltransport, pakketdienst)",
                                                           @"Groothandel (DeliXL, Hanos etc.)",
                                                           @"Instellingskeuken, restaurant, snackbar etc.",
                                                           @"Detailhandel (supermarkt, speciaalzaken, winkel aan huis, rijdende winkel, webshop)",
                                                           @"Loonwerker",
                                                           @"Toeleverancier (machines, veevoer, zaad- en pootgoed)",
                                                           @"Dienstverlener (adviseur, accountant, dierenarts)",
                                                           @"Anders",
                                                           nil];
}

@end
