//
//  DPNetworkService.m
//  Finance
//
//  Created by Admin on 20.05.16.
//  Copyright © 2016 diP. All rights reserved.
//

#import "NetworkService.h"
#import "HTTPSessionManager.h"
#import "AFNetworking/AFNetworking.h"



@interface NetworkService ()

@property (nonatomic, strong) HTTPSessionManager *sessionManager;

@end

@implementation NetworkService

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        self.sessionManager = [[HTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://app.foodpoints.nl/public/"]
                                                        sessionConfiguration:sessionConfiguration];
        self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
        self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        self.sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
    
    return self;
}

+ (instancetype)sharedService
{
    static NetworkService *networkRequestsService;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        networkRequestsService = [NetworkService new];
    });
    return networkRequestsService;
}

- (void)getApiWithCompletion:(NetworkRequestsBlock)completion
{
    
    
//    NSDictionary * params = @{ @"language" :@"ru",
//                               @"email":@"demouser@dalife.com",
//                               @"password":@"diamondalliance",
//                               @"offset":@"0",
//                               @"limit":@"5",
//                               @"format":@"json"};
    
    NSDictionary * params = @{};
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypeGET
                         URLString:@"api_test.php"
                        completion:completion];
}

- (void)registrationOfNewUserWithParams:(NSDictionary*)params andBlock:(NetworkRequestsBlock)completion
{

    
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/registration"
                        completion:completion];
}

- (void)loginOfCurrenUserWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;
{
   
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/login"
                        completion:completion];
}

- (void)getMarkersFromServerWithParams:(NSDictionary *) params andCompletition:(NetworkRequestsBlock)completion;
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/map"
                        completion:completion];
}

- (void)logoutOfcurrentUserWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/logout"
                        completion:completion];
}

- (void)getUserDataWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/profile"
                        completion:completion];
}

- (void)getSurveysListWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/surveys/list"
                        completion:completion];
}

- (void)createPreviewWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/survey/preview"
                        completion:completion];
}

- (void)saveCurrentSurveyWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/answers"
                        completion:completion];
}

- (void)getSurveysOfCurrentUserWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/results"
                        completion:completion];
}


- (void)getAnswersOfCurrenSurveyWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/results/company"
                        completion:completion];
}

- (void)downloadLinkOnYouTubeWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/company/youtube"
                        completion:completion];
}

- (void)getListOfSbiCodes:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/sbi/list"
                        completion:completion];
}

- (void)sendPreviewResult:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion
{
    [self getHTTPSessionWithParams:params
                              type:HTTPRequestTypePOST
                         URLString:@"api/user/company/can_survey"
                        completion:completion];
}

- (void)getHTTPSessionWithParams:(id)params
                            type:(HTTPRequestType)type
                       URLString:(NSString *)URLString
                      completion:(NetworkRequestsBlock)completion
{
    [self.sessionManager HTTPSessionWithHTTPRequestType:type
                                              URLString:URLString
                                             parameters:params
                                               progress:nil
                                                success:^(NSURLSessionDataTask *task, id responseObject)
     {
         if (completion)
         {
             completion(responseObject, nil);
         }
     }
                                                failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         if (completion)
         {
             completion(nil, error);
         }
     }];
}

@end
