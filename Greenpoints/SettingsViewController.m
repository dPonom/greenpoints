//
//  SettingsViewController.m
//  Greenpoints
//
//  Created by Dmitriy Ponomarenko on 14.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SettingsViewController.h"
#import "CustomCell.h"
#import "Singleton.h"
#import "APLSlideMenuViewController.h"

@interface SettingsViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *productTableView;
@property (weak, nonatomic) IBOutlet UITableView *activityTableView;

@property (strong, nonatomic) NSArray * productFilters;
@property (strong, nonatomic) NSArray * activityFilters;

@property (strong, nonatomic) NSMutableArray * tmpProducts;
@property (strong, nonatomic) NSMutableArray * tmpActivity;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addCornerRadiusToAllUIElemnts];
    
    self.tmpProducts = [[NSMutableArray alloc] init];
    self.tmpActivity = [[NSMutableArray alloc] init];
    
    self.productFilters = [[NSArray alloc] initWithObjects: @"Vlees",@"Vis",@"Groente",@"Aardappel",@"Fruit",@"Zuivel",@"Dranken /bier",@"Brood",@"Eierproducten",@"Gemaksvoeding / Snacks",@"Zoetwaren",@"Kruiden en preparaten", nil];
    
    //@"vlees",@"vis",@"groente",@"aardappel",@"fruit",@"zuivel",@"db",@"brood",@"eierproducten",@"gs",@"zoetwaren",@"kep"
    
    self.productTableView.editing = YES;
    self.activityTableView.editing = YES;
    
    self.activityFilters = [[NSArray alloc] initWithObjects:
                            @"Ruwe productie / teelt",@"Verwerking",@"Transport en opslag",@"Groothandel",@"Retail",@"Horeca",@"Zorginstelling",@"Overig", nil];
    
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Filter"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    
    
    //@"rpt",@"verwerking",@"teo",@"groothandel",@"retail",@"horeca",@"zorginstelling",@"overig"
    
//    Filter producten
//    Vlees
//    Vis
//    Groente
//    Aardappel
//    Fruit
//    Zuivel
//    Dranken /bier
//    Brood
//    Eierproducten
//    Gemaksvoeding / Snacks
//    Zoetwaren
//    Kruiden en preparaten
//    
//    Filter activiteiten
//    
//    Ruwe productie / teelt
//    Verwerking
//    Transport en opslag
//    Groothandel
//    Retail
//    Horeca
//    Zorginstelling
//    Overig
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) addCornerRadiusToAllUIElemnts
{
    self.searchTextField.layer.cornerRadius   = 10.f;
    self.filterButton.layer.cornerRadius      = 10.f;
    self.productTableView.layer.cornerRadius  = 10.f;
    self.activityTableView.layer.cornerRadius = 10.f;
    
    self.searchTextField.clipsToBounds   = YES;
    self.filterButton.clipsToBounds      = YES;
    self.productTableView.clipsToBounds  = YES;
    self.activityTableView.clipsToBounds = YES;
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    Singleton * sharedManager = [Singleton sharedManager];
    
    sharedManager.search = self.searchTextField.text;
    
    APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
    apl.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:apl animated:YES completion:nil];
    
    return YES;
    
}

#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.productTableView)
    {
        return self.productTableView.frame.size.height/8;
    }
    
    else
    {
        return self.productTableView.frame.size.height/8;
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.productTableView)
    {
        return [self.productFilters count];
    }
    else
    {
        return [self.activityFilters count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.productTableView)
    {
        CustomCell * cell = [tableView dequeueReusableCellWithIdentifier:@"product"];
        
        cell.textLabel.text = [self.productFilters objectAtIndex:indexPath.row];
        [cell.textLabel setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0]];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        return cell;
    }
    else
    {
        CustomCell * cell = [tableView dequeueReusableCellWithIdentifier:@"activity"];
        
        cell.textLabel.text = [self.activityFilters objectAtIndex:indexPath.row];
        [cell.textLabel setFont:[UIFont fontWithName:@"RobotoCondensed-Regular" size:16.0]];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.productTableView)
    {
        switch (indexPath.row)
        {
            case 0:
                [self.tmpProducts addObject:@"vlees"];
                break;
            case 1:
                [self.tmpProducts addObject:@"vis"];
                break;
            case 2:
                [self.tmpProducts addObject:@"groente"];
                break;
            case 3:
                [self.tmpProducts addObject:@"aardappel"];
                break;
            case 4:
                [self.tmpProducts addObject:@"fruit"];
                break;
            case 5:
                [self.tmpProducts addObject:@"zuivel"];
                break;
            case 6:
                [self.tmpProducts addObject:@"db"];
                break;
            case 7:
                [self.tmpProducts addObject:@"brood"];
                break;
            case 8:
                [self.tmpProducts addObject:@"eierproducten"];
                break;
            case 9:
                [self.tmpProducts addObject:@"gs"];
                break;
            case 10:
                [self.tmpProducts addObject:@"zoetwaren"];
                break;
            case 11:
                [self.tmpProducts addObject:@"kep"];
             
                
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
                [self.tmpActivity addObject:@"rpt"];
                break;
            case 1:
                [self.tmpActivity addObject:@"verwerking"];
                break;
            case 2:
                [self.tmpActivity addObject:@"teo"];
                break;
            case 3:
                [self.tmpActivity addObject:@"groothandel"];
                break;
            case 4:
                [self.tmpActivity addObject:@"retail"];
                break;
            case 5:
                [self.tmpActivity addObject:@"horeca"];
                break;
            case 6:
                [self.tmpActivity addObject:@"zorginstelling"];
                break;
            case 7:
                [self.tmpActivity addObject:@"overig"];
                break;
                
            default:
                break;
        }
    }
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.productTableView)
    {
        switch (indexPath.row)
        {
            case 0:
                [self.tmpProducts removeObject:@"vlees"];
                break;
            case 1:
                [self.tmpProducts removeObject:@"vis"];
                break;
            case 2:
                [self.tmpProducts removeObject:@"groente"];
                break;
            case 3:
                [self.tmpProducts removeObject:@"aardappel"];
                break;
            case 4:
                [self.tmpProducts removeObject:@"fruit"];
                break;
            case 5:
                [self.tmpProducts removeObject:@"zuivel"];
                break;
            case 6:
                [self.tmpProducts removeObject:@"db"];
                break;
            case 7:
                [self.tmpProducts removeObject:@"brood"];
                break;
            case 8:
                [self.tmpProducts removeObject:@"eierproducten"];
                break;
            case 9:
                [self.tmpProducts removeObject:@"gs"];
                break;
            case 10:
                [self.tmpProducts removeObject:@"zoetwaren"];
                break;
            case 11:
                [self.tmpProducts removeObject:@"kep"];
                
                
                break;
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
                [self.tmpActivity removeObject:@"rpt"];
                break;
            case 1:
                [self.tmpActivity removeObject:@"verwerking"];
                break;
            case 2:
                [self.tmpActivity removeObject:@"teo"];
                break;
            case 3:
                [self.tmpActivity removeObject:@"groothandel"];
                break;
            case 4:
                [self.tmpActivity removeObject:@"retail"];
                break;
            case 5:
                [self.tmpActivity removeObject:@"horeca"];
                break;
            case 6:
                [self.tmpActivity removeObject:@"zorginstelling"];
                break;
            case 7:
                [self.tmpActivity removeObject:@"overig"];
                break;
                
            default:
                break;
        }
    }
}
- (UITableViewCellEditingStyle)tableViewStyle
{
    return 3;
}

- (UITableViewCellEditingStyle)tableView:(UITableView * )tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.productTableView)
    {
        return [self tableViewStyle];
    }
    else
    {
        return [self tableViewStyle];
    }
}
- (IBAction)showButton:(id)sender
{
    Singleton * sharedManager = [Singleton sharedManager];
    
    [sharedManager.products addObjectsFromArray:self.tmpProducts];
    [sharedManager.activity addObjectsFromArray:self.tmpActivity];
    
    APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
    apl.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:apl animated:YES completion:nil];
    
}

@end
