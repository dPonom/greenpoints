//
//  ThirdQuestionViewController.h
//  Greenpoints
//
//  Created by Developer on 23.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThirdQuestionViewController : UIViewController

@property (strong, nonatomic) NSMutableArray * multipleAnswers;

@end
