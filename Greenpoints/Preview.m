//
//  Preview.m
//  Greenpoints
//
//  Created by Developer on 21.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "Preview.h"

@implementation Preview

+ (id)sharedManager {
    static Preview *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];

        sharedMyManager.company_id = [[NSString alloc] init];
        
    });
    return sharedMyManager;
}

@end
