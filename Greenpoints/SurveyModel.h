//
//  SurveyModel.h
//  Greenpoints
//
//  Created by Developer on 23.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SurveyModel : NSObject

@property (strong, nonatomic) NSString * question;
@property (strong, nonatomic) NSString * answer;


@end
