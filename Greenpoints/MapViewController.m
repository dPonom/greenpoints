//
//  MapViewController.m
//  Greenpoints
//
//  Created by Developer on 16.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "MapViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"
#import "NetworkService.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MagicalRecord/MagicalRecord.h>
#import "CurrentUser+CoreDataProperties.h"
#import "SbiCode+CoreDataProperties.h"
#import "SettingsViewController.h"
#import "MBProgressHUD.h"



@import GoogleMaps;

@interface MapViewController ()

@property (strong, nonatomic) NSArray * arrayOfPictures;
@property (strong, nonatomic) NSMutableArray * arrayOfmarkers;
@property (strong, nonatomic) NSDictionary * params;
@property (strong,nonatomic) NSMutableArray *companies;

@property (strong, nonatomic) NSString * searchString;



@end

@implementation MapViewController
{
    GMSMapView *mapView_;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getSettingsOfNavControoler];
    
    self.arrayOfmarkers = [[NSMutableArray alloc] init];
    
    Singleton * manager = [Singleton sharedManager];
    self.productFilters = manager.products;
    self.activityFilters = manager.activity;
    self.searchString = manager.search;
    
    NSDictionary * dict = @{@"0":self.productFilters,@"1":self.activityFilters};
    
    NSData * json = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString * jsonStr = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    
    if(self.searchString.length == 0)
    {
        self.params = @{@"filter":jsonStr,@"count":@"999999999"};
    }
    else
    {
        self.params = @{@"search":self.searchString,@"filter":jsonStr,@"count":@"99999999999"};
    }

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:53.177975
                                                            longitude:6.691623
                                                                 zoom:9];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    
    mapView_.myLocationEnabled = YES;
    
    self.view = mapView_;
    
    [self getMarkersFromServer];
    
    if(![SbiCode MR_findFirst])
    {
        [self getListOfSbiCodes];
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) getSettingsOfNavControoler
{
    self.slideMenuController.bouncing = YES;
    self.slideMenuController.gestureSupport = APLSlideMenuGestureSupportNone;
    self.slideMenuController.separatorColor = [UIColor grayColor];
    
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"GreenPoints"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"backForNavBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *imageSettings = [UIImage imageNamed:@"settingsIcon.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 23 ,23  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(settingsButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
    
    
    UIImage *image = [UIImage imageNamed:@"menu-button"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, 25 , 30);
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(plusButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *plusButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = plusButton;
    
    
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];

}

-(IBAction)settingsButton:(id)sender
{
    Singleton * manager = [Singleton sharedManager];
    
    [manager.products removeAllObjects];
    
    [manager.activity removeAllObjects];
    
    manager.search = @"";
    
    SettingsViewController * settings = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    settings.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    
    [self.navigationController pushViewController:settings animated:YES];
}

-(IBAction)plusButton:(id)sender
{
    [self.slideMenuController showLeftMenu:YES];
}

-(void) getMarkersFromServer
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = NSLocalizedString(@"Updating of the map", @"HUD loading title");
    
    [[NetworkService sharedService] getMarkersFromServerWithParams:self.params andCompletition:^(id responseObject, NSError *error)
     {
         if(responseObject)
         {
             [self mappingResponse:responseObject];
         }
         else
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }];
}

-(void) mappingResponse:(id) response
{
    for (id obj in response)
    {
        GMSMarker *marker = [[GMSMarker alloc] init];
        
        CGFloat latitude = [[obj objectForKey:@"lat"] floatValue];
        
        CGFloat longitude = [[obj objectForKey:@"lng"] floatValue];
        
        marker.position = CLLocationCoordinate2DMake(latitude, longitude);
        
        marker.snippet = [NSString stringWithFormat:@"Adr : %@\nyoutube - %@\nsbi - %@",[obj objectForKey:@"address"],[obj objectForKey:@"youtube_link"],[obj objectForKey:@"sbi"]];
        
        marker.title = [obj objectForKey:@"company_name"];
        
        NSString * name = [obj objectForKey:@"marker"];
        
        UIImage * image = [UIImage imageNamed:name];
        
        marker.icon = image;
        
        marker.appearAnimation = kGMSMarkerAnimationPop;
        
        marker.map = mapView_;
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void) getListOfSbiCodes
{
    CurrentUser * user = [CurrentUser MR_findFirst];
    
    NSDictionary * params = @{ @"user_id" : user.userID,
                               @"token"   : user.userToken };
    
    [[NetworkService sharedService] getListOfSbiCodes:params andBlock:^(id responseObject, NSError *error)
    {
        if([responseObject[@"success"] isEqual:@1])
        {
            for (id obj in responseObject[@"codes"])
            {
               [SbiCode MR_findFirstOrCreateByAttribute:@"codeID" withValue:obj];
            }
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        }
        else
        {
            NSLog(@"erroe of sbi codes");
        }
    }];
}

//-(void) addMarkers
//{
//    self.arrayOfmarkers  = [[NSMutableArray alloc] init];
//    self.arrayOfPictures = [[NSArray alloc] initWithObjects:@"apple_health",@"apple_infinity",@"apple_sale",@"apple_settings",@"bread_cultivation",
//                            @"bread_health",@"bread_infinity",@"bread_sale",@"bread_settings",@"cake_cultivation",
//                            @"cake_health",@"cake_infinity",@"cake_sale",@"cake_settings",@"carrot_cultivation",
//                            @"carrot_health",@"carrot_infinity",@"carrot_sale",@"carrot_settings",@"cheese_cultivation",
//                            @"cheese_health",@"cheese_infinity",@"cheese_sale",@"cheese_settings",@"drinks_cultivation",
//                            @"drinks_health",@"drinks_infinity",@"drinks_sale",@"drinks_settings",@"eggs_cultivation",
//                            @"eggs_health",@"eggs_infinity",@"eggs_sale",@"eggs_settings",@"fish_cultivation",
//                            @"fish_health",@"fish_infinity",@"fish_sale",@"fish_settings",@"herb_cultivation",
//                            @"herb_settings",@"herbs_health",@"herbs_infinity",@"herbs_sale",@"meat_cultivation",
//                            @"meat_health",@"meat_infinity",@"meat_sale",@"meat_settings",@"potato_cultivation",
//                            @"potato_health",@"potato_infinity",@"potato_sale",@"potato_settings",
//                            @"snacks_cultivation",@"snacks_health",@"snacks_infinity",@"snacks_sale",@"snacks_settings",nil];
//    
//    for (NSString * myImage in self.arrayOfPictures)
//    {
//        for (int x = 0 ; x < 5 ; x ++ )
//        {
//            float highLatitude = 52.047791;
//            float lowLatitude = 51.581345;
//            
//            float diff = highLatitude - lowLatitude;
//            float rndLatitude = (((float) rand() / RAND_MAX) * diff) + lowLatitude;
//            
//            float highLongitude = 5.968662;
//            float lowLongitude = 4.230341;
//            
//            float diffLong = highLongitude - lowLongitude;
//            float rndLongitude = (((float) rand() / RAND_MAX) * diffLong) + lowLongitude;
//            
//            GMSMarker *marker = [[GMSMarker alloc] init];
//            
//            marker.position = CLLocationCoordinate2DMake(rndLatitude, rndLongitude);
//            
//            marker.title = myImage;
//            
//            UIImage * image = [UIImage imageNamed:myImage];
//            
//            CGSize newSize = CGSizeMake(30, 64.28);
//            UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//            [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
//            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//            UIGraphicsEndImageContext();
//            
//            
//            
//            marker.icon = newImage;
//            
//            
//            
//            // marker.snippet = @"Ukraine";
//            
//            //marker.map = mapView_;
//            
//            
//            [self.arrayOfmarkers addObject:marker];
//            
//        }
//        
//    }
//    
//}

//-(void) drawMarkersOnMap
//{
//    NSString * currentFilter = [[NSString alloc] init];
//    NSString * currentActivity = [[NSString alloc] init];
//    
//    switch (self.filterOfProduct)
//    {
//        case 1:
//            currentFilter = @"meat";
//            break;
//        case 2:
//            currentFilter = @"apple";
//            break;
//        case 3:
//            currentFilter = @"bread";
//            break;
//        case 4:
//            currentFilter = @"cake";
//            break;
//        case 5:
//            currentFilter = @"carrot";
//            break;
//        case 6:
//            currentFilter = @"cheese";
//            break;
//        case 7:
//            currentFilter = @"drinks";
//            break;
//        case 8:
//            currentFilter = @"eggs";
//            break;
//        case 9:
//            currentFilter = @"fish";
//            break;
//        case 10:
//            currentFilter = @"herb";
//            break;
//        case 11:
//            currentFilter = @"potato";
//            break;
//        case 12:
//            currentFilter = @"snacks";
//            break;
//            
//        default:
//            break;
//    }
//    
//    switch (self.filterOfActvity)
//    {
//        case 1:
//            currentActivity = @"cultivation";
//            break;
//            
//        case 2:
//            currentActivity = @"settings";
//            break;
//            
//        case 3:
//            currentActivity = @"sale";
//            break;
//            
//        case 4:
//            currentActivity = @"health";
//            break;
//            
//        case 5:
//            currentActivity = @"infinity";
//            break;
//            
//            
//        default:
//            break;
//    }
//    
//    NSMutableArray * array = [[NSMutableArray alloc] init];
//    
//    for (GMSMarker * marker in self.arrayOfmarkers)
//    {
//        if(self.filterOfActvity && !self.filterOfProduct)
//        {
//            if([marker.title containsString:currentActivity])
//            {
//                [array addObject:marker];
//            }
//        }
//        
//        else if(!self.filterOfActvity && self.filterOfProduct)
//        {
//            if([marker.title containsString:currentFilter])
//            {
//                [array addObject:marker];
//            }
//        }
//        
//        else if (self.filterOfActvity && self.filterOfProduct)
//        {
//            if([marker.title containsString:currentActivity] && [marker.title containsString:currentFilter])
//            {
//                [array addObject:marker];
//            }
//        }
//        else
//        {
//            [array addObject:marker];
//        }
//        
//        
//    }
//    
//    //GMSMutablePath *path = [GMSMutablePath path];
//    NSLog(@"%lu",array.count);
//    for (GMSMarker * marker in array)
//    {
//        marker.map = mapView_;
//        //[path addCoordinate:marker.position];
//    }
// 
//}

//-(void) getMarkersFromServer
//{
//    [[NetworkService sharedService] getMarkersFromServerWithCompletition:^(id responseObject, NSError *error)
//    {
//        for (id obj in responseObject)
//        {
//            GMSMarker *marker = [[GMSMarker alloc] init];
//            
//            CGFloat latitude = [[obj objectForKey:@"lat"] floatValue];
//            CGFloat longitude = [[obj objectForKey:@"lng"] floatValue];
//            
//            marker.position = CLLocationCoordinate2DMake(latitude, longitude);
//            
//            marker.title = [obj objectForKey:@"company_name"];
//            
//            NSString * string = [NSString stringWithFormat:@"http://www.%@",[obj objectForKey:@"marker"]];
//            NSURL * url = [NSURL URLWithString:string];
//            NSData * data = [NSData dataWithContentsOfURL:url];
//            marker.icon = [UIImage imageWithData:data];
//      
//            marker.appearAnimation = kGMSMarkerAnimationPop;
//            marker.map = mapView_;
//            
//
//        }
//    }];
//}



@end


