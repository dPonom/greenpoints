//
//  ElevenQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ElevenQuestionViewController.h"
#import "TwelveQuestionViewController.h"
#import "PopoverViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface ElevenQuestionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *mainQuestionLabel;
- (IBAction)yesButton:(id)sender;
- (IBAction)noButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *yesDetailButton;
@property (weak, nonatomic) IBOutlet UITextField *detailTextField;
- (IBAction)nextButton:(id)sender;
- (IBAction)infoButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;

@property (strong,nonatomic) NSString * answer;
@property (assign,nonatomic) NSInteger value;

@end

@implementation ElevenQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)yesButton:(id)sender
{
    [self.yesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.noButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.yesDetailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
   
    self.value = 1;
}

- (IBAction)noButton:(id)sender
{
    [self.yesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.noButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.yesDetailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    

    self.value = 2;
}

- (IBAction)yesDetailButton:(id)sender
{
    [self.yesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.noButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.yesDetailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    self.value = 3;
}

- (IBAction)nextButton:(id)sender
{
    switch (self.value)
    {
        case 1:
            self.answer = @"Ik wil niets leren";
            break;
        case 2:
             self.answer = @"Weet ik niet";
            break;
        case 3:
            self.answer = [NSString stringWithFormat:@"Ja - %@",self.detailTextField.text];
            break;
            
        default:
            break;
    }
    NSDictionary * dict = @{@"question":@"11. Zou u graag iets van andere ondernemers willen leren?",
                            @"answer":@[self.answer]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:11];
    
    TwelveQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TwelveQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 9;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
