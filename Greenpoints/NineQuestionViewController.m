//
//  NineQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "NineQuestionViewController.h"
#import "TenQuestionViewController.h"
#import "PopoverViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface NineQuestionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *mainQuestionLabel;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UITextField *yesTextField;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *dontKnowButton;
- (IBAction)yesButton:(id)sender;
- (IBAction)noButton:(id)sender;
- (IBAction)dontKnowButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *troublesLabel;
@property (weak, nonatomic) IBOutlet UITextField *troublesTextField;
- (IBAction)nextbutton:(id)sender;
- (IBAction)infoButton:(id)sender;

@property (strong, nonatomic) NSString * answer;
@property (assign,nonatomic) NSInteger value;

@end

@implementation NineQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    self.yesTextField.hidden = YES;
    self.troublesLabel.hidden = YES;
    self.troublesTextField.hidden = YES;
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark Keyboard Settings

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification object:nil];
}

// Unsubscribe from keyboard show/hide notifications.
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWillShow:(NSNotification*)notification
{
    [self moveView:[notification userInfo] up:YES];
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    [self moveView:[notification userInfo] up:NO];
}

- (void)moveView:(NSDictionary*)userInfo up:(BOOL)up
{
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]
     getValue:&keyboardEndFrame];
    
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey]
     getValue:&animationCurve];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey]
     getValue:&animationDuration];
    
    // Get the correct keyboard size to we slide the right amount.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    int y = keyboardFrame.size.height * (up ? -1 : 1);
    self.view.frame = CGRectOffset(self.view.frame, 0, y);
    
    [UIView commitAnimations];
}


- (IBAction)yesButton:(id)sender
{
    self.yesTextField.hidden = NO;
    self.troublesLabel.hidden = NO;
    self.troublesTextField.hidden = NO;
    
    [self.yesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.noButton  setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.dontKnowButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.troublesLabel setTextColor:[UIColor blackColor]];
    
    self.value = 1;
    
}

- (IBAction)noButton:(id)sender
{
    self.yesTextField.hidden = YES;
    self.troublesLabel.hidden = YES;
    self.troublesTextField.hidden = YES;
    
    [self.yesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.noButton  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.dontKnowButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.troublesLabel setTextColor:[UIColor blackColor]];
    
    self.value = 2;
    
}

- (IBAction)dontKnowButton:(id)sender
{
    self.yesTextField.hidden = YES;
    self.troublesLabel.hidden = YES;
    self.troublesTextField.hidden = YES;
    
    [self.yesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.noButton  setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.dontKnowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.troublesLabel setTextColor:[UIColor blackColor]];
    
    self.value = 3;
    
}

- (IBAction)nextbutton:(id)sender
{
    switch (self.value)
    {
        case 1:
            self.answer = [NSString stringWithFormat:@"Ja - %@",self.yesTextField.text];
            break;
        case 2:
            self.answer = @"Nee";
            break;
        case 3:
            self.answer = @"Weet ik niet";
            break;
            
        default:
            break;
    }
    
    NSDictionary * dict = @{@"question":@"9. Bent u bezig met maatschappelijke doelen (koeien in de wei, gezond produceren, duurzaam, leveren aan regionale afnemers etc.)?",
                            @"answer":@[self.answer]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:9];
    
    TenQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TenQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 7;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
