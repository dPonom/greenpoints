//
//  ProfileViewController.m
//  Greenpoints
//
//  Created by Dmitriy Ponomarenko on 19.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ProfileViewController.h"
#import "APLSlideMenuViewController.h"
#import <MagicalRecord/MagicalRecord.h>
#import "NetworkService.h"
#import "LoginViewController.h"
#import "Singleton.h"
#import "CurrentUser+CoreDataProperties.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface ProfileViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
@property (weak, nonatomic) IBOutlet UILabel *educationTypeLabel;
@property (strong,nonatomic) UIImage * image;

@property (strong,nonatomic) NSString * token;
@property (assign,nonatomic) NSInteger  user_id;

@property (weak, nonatomic) IBOutlet UIButton *photoButton;

@end

@implementation ProfileViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.emailLabel.adjustsFontSizeToFitWidth = YES;
    
    self.photoButton.layer.cornerRadius = 10.f;
    self.photoButton.clipsToBounds = YES;
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Mijn profiel"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"backForNavBar.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    
    UIImage *image = [UIImage imageNamed:@"backButton2.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake( 0, 0, 25 , 25);
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *plusButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = plusButton;
    
    //CurrentUser
    
    [self downloadUserProfileInfo];
   
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoImageView.layer.cornerRadius = 79;
    self.photoImageView.layer.masksToBounds = YES;
}

-(void) downloadUserProfileInfo
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = NSLocalizedString(@"Loading info...", @"HUD loading title");
    
    CurrentUser * user = [CurrentUser MR_findFirst];
    
    NSDictionary * params = @{@"user_id" : user.userID,
                              @"token"   : user.userToken };
    
    [[NetworkService sharedService] getUserDataWithParams:params
                                                 andBlock:^(id responseObject, NSError *error)
     {
         if (responseObject[@"profile"])
         {
             if([[responseObject[@"profile"] objectForKey:@"fullname"] isEqual:[NSNull null]] ||
                [[responseObject[@"profile"] objectForKey:@"school"] isEqual:[NSNull null]])
             {
                 self.nameLabel.text   = @"empty" ;
                 self.schoolLabel.text = @"empty" ;
             }
             else
             {
                 self.nameLabel.text          = [responseObject[@"profile"] objectForKey:@"fullname"];
                 self.schoolLabel.text        = [responseObject[@"profile"] objectForKey:@"school"];
             }
    
             self.emailLabel.text         = [responseObject[@"profile"] objectForKey:@"email"];
             self.educationTypeLabel.text = [responseObject[@"profile"] objectForKey:@"title"];
             
             [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:[responseObject[@"profile"] objectForKey:@"profile_pic"]]];
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
         else
         {
             NSLog(@"Error");
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)photoButton:(id)sender
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    // Dismiss the image selection, hide the picker and
    
    //show the image view with the picked image
    self.image = image;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.photoImageView setImage:self.image];
}

-(IBAction)backButton:(id)sender
{
    APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
    [self presentViewController:apl animated:YES completion:nil];
}

- (IBAction)logoutButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Attention"
                                                                   message:@"Do you really want logout current account ?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Log Out" style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) { [self logOutCurrentAccount];}];
    UIAlertAction* cancel        = [UIAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              
                                                          }];
    
    [alert addAction:defaultAction];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) logOutCurrentAccount
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    CurrentUser * user = [CurrentUser MR_findFirst];
    
    [[NetworkService sharedService] logoutOfcurrentUserWithParams:@{@"user_id"  : user.userID,
                                                                    @"token"    : user.userToken }
                                                         andBlock:^(id responseObject, NSError *error)
    {
        if([responseObject[@"success"] isEqual:@(1)])
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [CurrentUser MR_truncateAll];
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
            
            LoginViewController * log = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [self presentViewController:log animated:YES completion:nil];
        }
        else
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"Error of logout");
        }
    }];
}

@end
