//
//  Singleton.h
//  Greenpoints
//
//  Created by Dmitriy Ponomarenko on 07.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject

@property (nonatomic, retain) NSMutableArray * products;
@property (nonatomic, retain) NSMutableArray * activity;
@property (nonatomic, retain) NSMutableArray * answers;
@property (strong, nonatomic) NSString * search;

@property (strong, nonatomic) NSString * user_id;
@property (strong, nonatomic) NSString * token;

+ (id)sharedManager;

@end
