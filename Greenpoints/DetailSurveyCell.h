//
//  DetailSurveyCell.h
//  Greenpoints
//
//  Created by Developer on 22.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailSurveyCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;

+(CGFloat) heightForText:(NSString*) text;

@end
