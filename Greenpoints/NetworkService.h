//
//  DPNetworkService.h
//  Finance
//
//  Created by Admin on 20.05.16.
//  Copyright © 2016 diP. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NetworkRequestsBlock)(id responseObject, NSError *error);

@interface NetworkService : NSObject

+ (instancetype)sharedService;

- (void)getApiWithCompletion:(NetworkRequestsBlock)completion;

- (void)registrationOfNewUserWithParams:(NSDictionary*)params andBlock:(NetworkRequestsBlock)completion;

- (void)loginOfCurrenUserWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

- (void)getMarkersFromServerWithParams:(NSDictionary *) params andCompletition:(NetworkRequestsBlock)completion;

- (void)logoutOfcurrentUserWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

- (void)getUserDataWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

- (void)getSurveysListWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

- (void)createPreviewWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

- (void)saveCurrentSurveyWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

- (void)getSurveysOfCurrentUserWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

- (void)getAnswersOfCurrenSurveyWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

- (void)downloadLinkOnYouTubeWithParams:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

#pragma mark ListOfSBICodes

- (void)getListOfSbiCodes:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;

#pragma mark SendPreviewResultToServer

- (void)sendPreviewResult:(NSDictionary *) params andBlock:(NetworkRequestsBlock)completion;




@end
