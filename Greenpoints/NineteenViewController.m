//
//  NineteenViewController.m
//  Greenpoints
//
//  Created by Developer on 31.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "NineteenViewController.h"
#import "PopoverViewController.h"
#import "TwentyViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface NineteenViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
- (IBAction)noButton:(id)sender;
- (IBAction)yesButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UITextField *websiteTextField;
@property (weak, nonatomic) IBOutlet UITextField *linkedinTextField;
@property (weak, nonatomic) IBOutlet UITextField *facebookTextField;
@property (weak, nonatomic) IBOutlet UITextField *elseTextField;
- (IBAction)nextButton:(id)sender;
- (IBAction)infoButton:(id)sender;

@property (strong, nonatomic) NSString * answer;
@property (assign,nonatomic) NSInteger value;

@end

@implementation NineteenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    self.websiteTextField.hidden = YES;
    self.linkedinTextField.hidden = YES;
    self.facebookTextField.hidden = YES;
    self.elseTextField.hidden = YES;
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)noButton:(id)sender
{
    [self.noButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.yesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    self.websiteTextField.hidden = YES;
    self.linkedinTextField.hidden = YES;
    self.facebookTextField.hidden = YES;
    self.elseTextField.hidden = YES;
    
    self.value = 1;
}

- (IBAction)yesButton:(id)sender
{
    [self.noButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.yesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.websiteTextField.hidden = NO;
    self.linkedinTextField.hidden = NO;
    self.facebookTextField.hidden = NO;
    self.elseTextField.hidden = NO;
   
    self.value = 2;
}

- (IBAction)nextButton:(id)sender
{
    switch (self.value)
    {
        case 1:
            self.answer = @"Nee";

            break;
        case 2:
            self.answer = [NSString stringWithFormat:@"Ja - %@,%@,%@,%@",self.websiteTextField.text,self.linkedinTextField.text,self.facebookTextField.text,self.elseTextField.text];
            break;
            
        default:
            break;
    }
    NSDictionary * dict = @{@"question":@"19. Bent u actief op (social) media?",
                            @"answer":@[self.answer]};
    
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:19];
    
    TwentyViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TwentyViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 15;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
