//
//  EightQuestionViewController.m
//  Greenpoints
//
//  Created by Developer on 25.08.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "EightQuestionViewController.h"
#import "NineQuestionViewController.h"
#import "PopoverViewController.h"
#import "DetailsViewController.h"
#import "APLSlideMenuViewController.h"
#import "Singleton.h"

@interface EightQuestionViewController ()
@property (weak, nonatomic) IBOutlet UILabel *mainQuestionLabel;
@property (weak, nonatomic) IBOutlet UIButton *gangBtn;
@property (weak, nonatomic) IBOutlet UIButton *bioBtn;
@property (weak, nonatomic) IBOutlet UIButton *andersBtn;
- (IBAction)gangBtn:(id)sender;
- (IBAction)BioBtn:(id)sender;
- (IBAction)andersBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *detailTextField;
- (IBAction)nextButton:(id)sender;
- (IBAction)infoButton:(id)sender;
- (IBAction)questionButton:(id)sender;

@property (strong, nonatomic) NSString * answer;


@end

@implementation EightQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.detailTextField.hidden = YES;
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIImage *imageSettings = [UIImage imageNamed:@"exitButton2.png"];
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSettings.bounds = CGRectMake( 0, 0, 22 ,22  );
    [buttonSettings setImage:imageSettings forState:UIControlStateNormal];
    [buttonSettings addTarget:self action:@selector(exitButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithCustomView:buttonSettings];
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(IBAction)exitButton:(id)sender
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Waarschuwing!"
                                                                   message:@"Weet je zeker dat je de vragenlijst wilt sluiten? De door jouw ingevoerde gegevens zullen dan niet worden opgeslagen."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        APLSlideMenuViewController * apl = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
                                        [self presentViewController:apl animated:YES completion:nil];
                                    }];
    UIAlertAction* exitAction = [UIAlertAction actionWithTitle:@"Annuleren" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
    
    [alert addAction:defaultAction];
    [alert addAction:exitAction];
    [self presentViewController:alert animated:YES completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)gangBtn:(id)sender
{
    self.detailTextField.hidden = YES;
    [self.gangBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.bioBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.andersBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    self.answer = @"Gangbaar";
}

- (IBAction)BioBtn:(id)sender
{
     self.detailTextField.hidden = YES;
    [self.gangBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.bioBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.andersBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    self.answer = @"Biologisch";
}

- (IBAction)andersBtn:(id)sender
{
    self.detailTextField.hidden = NO;
    [self.gangBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.bioBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.andersBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.answer = @"Anders";
}

- (IBAction)nextButton:(id)sender
{
    NSDictionary * dict = @{@"question":@"8. Volgens welke methode produceert u?",
                            @"answer":@[self.answer]};
    [[[Singleton sharedManager] answers] setObject:dict atIndexedSubscript:8];
    
    NineQuestionViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NineQuestionViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)infoButton:(id)sender
{
    PopoverViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PopoverViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 6;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)questionButton:(id)sender
{
    DetailsViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.number = 3;
    [self presentViewController:vc animated:YES completion:nil];
}
@end
