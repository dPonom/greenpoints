//
//  TryDateViewController.m
//  Greenpoints
//
//  Created by павев on 04.09.16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "TryDateViewController.h"
#import "CompanyInfoViewController.h"
#import "WhyFailedViewController.h"
#import "APLSlideMenuViewController.h"
#import "NetworkService.h"
#import <MagicalRecord/MagicalRecord.h>
#import "CurrentUser+CoreDataProperties.h"
#import "Company+CoreDataProperties.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface TryDateViewController () < UITableViewDelegate, UITableViewDataSource >

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray * dataTable;
@property (assign, nonatomic) NSInteger status;
@property (strong, nonatomic) NSIndexPath *selectionIndexPath;

@end

@implementation TryDateViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataTable = @[@"Ja, en gelukt",@"Ja, en niet gelukt",@"Nee"];
    self.tableView.scrollEnabled = NO;
    [self.tableView setEditing:YES];
    
    
    UIFont * font = [[UIFont alloc] init];
    font = [UIFont fontWithName:@"RobotoCondensed-Regular" size:25.0];
    
    [self.navigationItem setTitle:@"Vragenlijst"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:font}];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.tableView.frame.size.height / 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"customCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [self.dataTable objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.selectionIndexPath.row != indexPath.row)
    {
        [self.tableView deselectRowAtIndexPath:self.selectionIndexPath animated:NO];
    }
   
    self.selectionIndexPath = indexPath;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableViewStyle];
}

- (UITableViewCellEditingStyle)tableViewStyle
{
    return 3;
}


- (IBAction)nextButton:(id)sender
{
    self.status = self.selectionIndexPath.row + 1;
    
    CompanyInfoViewController * companyInfo;
    WhyFailedViewController * whyFailed;
    switch (self.status) {
        case 1:
            NSLog (@"success"); //Ja, en gelukt 
            companyInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"CompanyInfoViewController"];
            [self.navigationController pushViewController:companyInfo animated:YES];
            break;
        case 2:
            NSLog (@"not success yet"); //Ja, en niet gelukt 
            whyFailed = [self.storyboard instantiateViewControllerWithIdentifier:@"WhyFailedViewController"];
            [self.navigationController pushViewController:whyFailed animated:YES];
            break;
        case 3:
            NSLog (@"failed"); // Nee
            [self savingPreviewToServer];
            break;
            
        default:
            return;
            break;
    }
}

-(void) savingPreviewToServer
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = NSLocalizedString(@"Saving Preview", @"HUD loading title");
    
    CurrentUser * user = [CurrentUser MR_findFirst];
    Company * company  = [[Company MR_findAll] lastObject];
    
    NSDictionary * params = @{@"user_id"       : user.userID,
                              @"token"         : user.userToken,
                              @"company_id"    : company.companyID,
                              @"survey_status" : @"Nee een afspraak te maken met het bedrijf" };
    [[NetworkService sharedService] sendPreviewResult:params andBlock:^(id responseObject, NSError *error)
    {
        if([responseObject[@"success"] isEqual:@1])
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            APLSlideMenuViewController * aplslide = [self.storyboard instantiateViewControllerWithIdentifier:@"APLSlideMenuViewController"];
            [self presentViewController:aplslide animated:YES completion:nil];
        }
        else
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"error of sending preview to server");
        }
    }];
}

@end
